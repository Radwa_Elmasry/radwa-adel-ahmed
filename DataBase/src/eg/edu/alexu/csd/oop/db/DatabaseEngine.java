package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;

public class DatabaseEngine implements Database {
	private SqlParser parser = SqlParser.getInstance();

	public DatabaseEngine() {
		CreatingAndDroping drop = new CreatingAndDroping();
		drop.dropDatabase(parser.getDbName(), true);
		parser.setDbName(null);
	}

	@Override
	public String createDatabase(String databaseName, boolean dropIfExists) {

		if (dropIfExists) {
			try {
				executeStructureQuery("DROP DATABASE " + databaseName);
				executeStructureQuery("CREATE DATABASE " + databaseName);
			} catch (SQLException e) {

				e.printStackTrace();
			}

		} else {
			try {
				executeStructureQuery("CREATE DATABASE " + databaseName);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		SavingPath returnPath = new SavingPath();
		String path = returnPath.savingPath(databaseName);
		return path;
	}

	@Override
	public boolean executeStructureQuery(String query) throws SQLException {

		//log(query, false);
		if (query.toLowerCase().contains("table")) {
			if (parser.getDbName() == null)
				throw new RuntimeException();
		}
		boolean valid = parser.sqlParserCreate(query);
		if (!valid) {
			throw new SQLException();
		}
		boolean returnBoolean = parser.iscreateAndDrop();
		parser.setcreateAndDrop(false);

		return (returnBoolean);
	}

	@Override
	public Object[][] executeQuery(String query) throws SQLException {
		//log(query, false);
		boolean valid = parser.sqlParserSelect(query);
		if (!valid) {
			throw new SQLException();
		}
		return parser.getSelected();
	}

	@Override
	public int executeUpdateQuery(String query) throws SQLException {
		//log(query, true);
		boolean valid = parser.sqlParserUpdate(query);
		if (!valid) {
			throw new SQLException();
		}
		int returnInt = 0;

		if (query.toLowerCase().contains("delete")) {
			returnInt = parser.getDeleteCount();
			parser.setDeleteCount(0);
		}

		if (query.toLowerCase().contains("insert")) {
			returnInt = parser.getInsertCount();
			parser.setInsertCount(0);
		}

		if (query.toLowerCase().contains("update")) {
			returnInt = parser.getUpdateCount();
			parser.setUpdateCount(0);
		}

		return returnInt;
	}

	private static final String FILE_NAME = "/debug/aliaa.log";

	private static void log(String str, boolean delete) {
		try {
			if (delete)
				new File(FILE_NAME).delete();
			java.nio.file.Files.write(java.nio.file.Paths.get(FILE_NAME), str.getBytes(),
					new File(FILE_NAME).exists() ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
		} catch (Throwable e1) {
			e1.printStackTrace();
		}
	}

}