package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlReader {
	public ArrayList<String[]> readXml(String savingPath, String[] colsName) {

		ArrayList<String[]> table = new ArrayList<String[]>();
		try {
			
			File fXmlFile = new File(savingPath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getDocumentElement().getChildNodes();
			System.out.println(nList.getLength());
			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String idRow = eElement.getAttribute("id");
					String[] row = new String[colsName.length];
					for (int j = 0; j < colsName.length; j++) {
						row[j] = eElement.getElementsByTagName(colsName[j].toLowerCase()).item(0).getTextContent();
					}
					table.add(row);
				}
				
			}
		} catch (Exception e) {
			 throw new RuntimeException();
		}

		return table;
	}
}
