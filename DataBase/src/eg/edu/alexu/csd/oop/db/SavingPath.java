package eg.edu.alexu.csd.oop.db;

import java.io.File;

public class SavingPath {

	public String savingPath(String name) {

		String string = System.getProperty("java.class.path");
		String[] strar = string.split(File.pathSeparator);
		String path = strar[0] + "\\" + name;
        // String savingPath =  "/debug/" + name ;
		return path;
	}

	public boolean CheckFileExistence(String path) {

		return new File(path).isFile();

	}


}
