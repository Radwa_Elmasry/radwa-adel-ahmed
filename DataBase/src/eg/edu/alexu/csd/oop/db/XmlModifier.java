package eg.edu.alexu.csd.oop.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class XmlModifier {

	private XmlReader read = new XmlReader();
	private int countDelete = 0;
	private int countUpdate = 0;

	public ArrayList<String[]> insert(String[] colsName, String[] contents, String savingPath) {
		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		tableLoaded.add(contents);

		return tableLoaded;
	}

	public ArrayList<String[]> update(String[] colsName, String savingPath, String[] columns, String where) {
		countUpdate = 0;
		String[] arr = null;
		String colName, value, sign;
		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		ArrayList<String[]> required = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		where = where.replaceAll(" ", "");
		arr = where.split("((?<=\\=)|(?=\\=)|(?<=\\>)|(?=\\>)|(?<=\\<)|(?=\\<))");
		colName = arr[0];
		sign = arr[1];
		value = arr[2];

		int selectedIndex = 0;
		for (int i = 0; i < colsName.length; i++) {
			if (colsName[i].equalsIgnoreCase(colName)) {
				selectedIndex = i;
			}
		}
		for (int i = 0; i < columns.length; i++) {
			columns[i] = columns[i].replaceAll(" ", "");
			columns[i] = columns[i].replaceAll("'", "");
			arr = columns[i].split("=");
			colName = arr[0];
			String value2 = arr[1];
			for (int j = 0; j < colsName.length; j++) {
				if (colsName[j].equalsIgnoreCase(colName)) {
					String[] temp = new String[2];
					temp[0] = Integer.toString(j);
					temp[1] = value2;
					required.add(temp);
				}
			}
		}
		for (int i = 0; i < tableLoaded.size(); i++) {
			if (sign.equals("=")) {
				if (tableLoaded.get(i)[selectedIndex].equalsIgnoreCase(value)) {
					for (int j = 0; j < required.size(); j++) {
						int index = Integer.parseInt(required.get(j)[0]);
						tableLoaded.get(i)[index] = required.get(j)[1];

					}
					countUpdate++;
				}
			}
			if (sign.equals(">")) {
				if (Integer.valueOf(tableLoaded.get(i)[selectedIndex]) > Integer.valueOf(value)) {
					for (int j = 0; j < required.size(); j++) {
						int index = Integer.parseInt(required.get(j)[0]);
						tableLoaded.get(i)[index] = required.get(j)[1];

					}
					countUpdate++;
				}
			}
			if (sign.equals("<")) {
				if (Integer.valueOf(tableLoaded.get(i)[selectedIndex]) < Integer.valueOf(value)) {
					for (int j = 0; j < required.size(); j++) {
						int index = Integer.parseInt(required.get(j)[0]);
						tableLoaded.get(i)[index] = required.get(j)[1];

					}
					countUpdate++;
				}
			}
		}
		return tableLoaded;
	}

	public ArrayList<String[]> updateWithoutWhere(String[] colsName, String savingPath, String[] columns) {
		countUpdate = 0;
		String[] arr = null;
		String colName;
		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		ArrayList<String[]> required = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		for (int i = 0; i < columns.length; i++) {
			columns[i] = columns[i].replaceAll(" ", "");
			columns[i] = columns[i].replaceAll("'", "");
			arr = columns[i].split("=");
			colName = arr[0];
			String value2 = arr[1];
			for (int j = 0; j < colsName.length; j++) {
				if (colsName[j].equalsIgnoreCase(colName)) {
					String[] temp = new String[2];
					temp[0] = Integer.toString(j);
					temp[1] = value2;
					required.add(temp);
				}
			}
		}
		for (int i = 0; i < tableLoaded.size(); i++) {

			for (int j = 0; j < required.size(); j++) {
				int index = Integer.parseInt(required.get(j)[0]);
				tableLoaded.get(i)[index] = required.get(j)[1];

			}
			countUpdate++;
		}

		return tableLoaded;
	}

	public ArrayList<String[]> delete(String[] colsName, String savingPath, String s) {
		countDelete = 0;
		String colName, value, sign;
		String[] arr = null;
		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		s = s.replaceAll(" ", "");
		arr = s.split("((?<=\\=)|(?=\\=)|(?<=\\>)|(?=\\>)|(?<=\\<)|(?=\\<))");

		colName = arr[0];
		sign = arr[1];
		value = arr[2];
		int index = 0;
		for (int i = 0; i < colsName.length; i++) {
			if (colsName[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}

		for (int i = 0; i < tableLoaded.size(); i++) {
			if (sign.equals("=")) {
				if (tableLoaded.get(i)[index].equalsIgnoreCase(value)) {
					tableLoaded.remove(i);
					countDelete++;
					i--;
				}
			}
			if (sign.equals(">")) {
				if (Integer.valueOf(tableLoaded.get(i)[index]) > Integer.valueOf(value)) {
					tableLoaded.remove(i);
					countDelete++;
					i--;
				}
			}
			if (sign.equals("<")) {
				if (Integer.valueOf(tableLoaded.get(i)[index]) < Integer.valueOf(value)) {
					tableLoaded.remove(i);
					countDelete++;
					i--;
				}
			}
		}

		return tableLoaded;
	}

	public ArrayList<String[]> deleteAll(String[] colsName, String savingPath) {
		countDelete = 0;

		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		countDelete = tableLoaded.size();
		tableLoaded.clear();

		return tableLoaded;
	}

	public Object[][] selectAll(String[] colsName, String savingPath) {

		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		Object[][] selected = new Object[tableLoaded.size()][tableLoaded.get(0).length];

		for (int i = 0; i < tableLoaded.size(); i++) {

			for (int j = 0; j < tableLoaded.get(i).length; j++) {

				selected[i][j] = tableLoaded.get(i)[j];

			}

		}

		return selected;
	}

	public Object[][] selectCols(String[] colsName, String savingPath, String[] columnNames) {

		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		int[] indexes = new int[columnNames.length];
		for (int i = 0; i < columnNames.length; i++) {
			for (int j = 0; j < colsName.length; j++) {
				if (columnNames[i].equalsIgnoreCase(colsName[j])) {
					indexes[i] = j;
				}
			}
		}

		Object[][] selected = new Object[tableLoaded.size()][columnNames.length];

		for (int i = 0; i < tableLoaded.size(); i++) {

			for (int j = 0; j < indexes.length; j++) {

				selected[i][j] = tableLoaded.get(i)[indexes[j]];

			}

		}

		return selected;
	}

	public Object[][] selectAllConditional(String[] colsName,
			   String savingPath, String s) {

			  int countRows = 0;
			  ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
			  try {
			   tableLoaded = read.readXml(savingPath, colsName);
			  } catch (Exception e) {
			   throw new RuntimeException();
			  }

			  s = s.replaceAll(" ", "");
			  String[] arr = s
			    .split("((?<=\\=)|(?=\\=)|(?<=\\>)|(?=\\>)|(?<=\\<)|(?=\\<))");

			  String colName = arr[0];
			  String sign = arr[1];
			  String value = arr[2];

			  int index = 0;
			  for (int i = 0; i < colsName.length; i++) {
			   if (colsName[i].equalsIgnoreCase(colName)) {
			    index = i;
			   }
			  }

			  Object[][] selected = new Object[tableLoaded.size()][tableLoaded.get(0).length];

			  for (int i = 0; i < tableLoaded.size(); i++) {
			   if (sign.equals("=")) {
			    if (tableLoaded.get(i)[index].equalsIgnoreCase(value)) {

			     for (int j = 0; j < tableLoaded.get(i).length; j++) {

			      if(isDigit(tableLoaded.get(i)[j])){
			      selected[i][j] = Integer.parseInt(tableLoaded.get(i)[j]);
			      }
			      else
			      {
			       selected[i][j] = tableLoaded.get(i)[j];
			      }
			      }

			     }
			     countRows++;
			    }
			    
			   

			   if (sign.equals(">")) {
			    if (Integer.valueOf(tableLoaded.get(i)[index]) > Integer
			      .valueOf(value)) {
			     for (int j = 0; j < tableLoaded.get(i).length; j++) {

			      if(isDigit(tableLoaded.get(i)[j])){
			       selected[i][j] = Integer.parseInt(tableLoaded.get(i)[j]);
			       }
			       else
			       {
			        selected[i][j] = tableLoaded.get(i)[j];
			       }
			     }
			     countRows++;
			    }
			    
			   }
			   if (sign.equals("<")) {
			    if (Integer.valueOf(tableLoaded.get(i)[index]) < Integer
			      .valueOf(value)) {
			     for (int j = 0; j < tableLoaded.get(i).length; j++) {

			      if(isDigit(tableLoaded.get(i)[j])){
			       selected[i][j] = Integer.parseInt(tableLoaded.get(i)[j]);
			       }
			       else
			       {
			        selected[i][j] = tableLoaded.get(i)[j];
			       }

			     }
			     countRows++;
			    }
			    
			   }

			  }
			  Object[][] finalSelected = new Object[countRows][colsName.length];

			  for (int i = 0; i < countRows; i++) {
			   for (int j = 0; j < colsName.length; j++) {
			    finalSelected[i][j] = selected[i][j];
			   }
			  }

			  return finalSelected;
			 
			}

	public Object[][] selectColsConditional(String[] colsName, String[] columnNames, String savingPath, String s) {

		ArrayList<String[]> tableLoaded = new ArrayList<String[]>();
		try {
			tableLoaded = read.readXml(savingPath, colsName);
		} catch (Exception e) {
			throw new RuntimeException();
		}

		s = s.replaceAll(" ", "");
		String[] arr = s.split("((?<=\\=)|(?=\\=)|(?<=\\>)|(?=\\>)|(?<=\\<)|(?=\\<))");

		String colName = arr[0];
		String sign = arr[1];
		String value = arr[2];

		int index = 0;
		for (int i = 0; i < colsName.length; i++) {
			if (colsName[i].equalsIgnoreCase(colName)) {
				index = i;
			}
		}

		int[] indexes = new int[columnNames.length];
		for (int i = 0; i < columnNames.length; i++) {
			for (int j = 0; j < colsName.length; j++) {
				if (columnNames[i].equalsIgnoreCase(colsName[j])) {
					indexes[i] = j;
				}
			}
		}

		Object[][] selectedTemp = new Object[tableLoaded.size()][columnNames.length];
		int countRows = 0;
		for (int i = 0; i < tableLoaded.size(); i++) {
			if (sign.equals("=")) {
				if (tableLoaded.get(i)[index].equalsIgnoreCase(value)) {

					for (int j = 0; j < indexes.length; j++) {

						selectedTemp[i][j] = tableLoaded.get(i)[indexes[j]];

					}
					countRows++;
				}
			}

			if (sign.equals(">")) {
				if (Integer.valueOf(tableLoaded.get(i)[index]) > Integer.valueOf(value)) {
					for (int j = 0; j < indexes.length; j++) {

						selectedTemp[i][j] = tableLoaded.get(i)[indexes[j]];

					}
					countRows++;
				}
			}
			if (sign.equals("<")) {
				if (Integer.valueOf(tableLoaded.get(i)[index]) < Integer.valueOf(value)) {
					for (int j = 0; j < indexes.length; j++) {

						selectedTemp[i][j] = tableLoaded.get(i)[indexes[j]];

					}
					countRows++;
				}
			}
		}

		Object[][] selected = new Object[countRows][columnNames.length];

		for (int i = 0; i < countRows; i++) {
			for (int j = 0; j < columnNames.length; j++) {
				selected[i][j] = selectedTemp[i][j];
			}
		}

		return selected;
	}

	public int getCountDelete() {
		return countDelete;
	}

	public int getCountUpdate() {
		return countUpdate;
	}
	
	 public boolean isDigit(String name) {
	     return name.matches("[0-9]+");
	 }

}
