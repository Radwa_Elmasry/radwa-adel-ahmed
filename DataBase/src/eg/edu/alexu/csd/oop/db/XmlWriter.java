package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlWriter {

	public boolean  writeXml(String[] colsName,  String tableName, String savingPath , ArrayList<String[]> tableLoaded) {
		boolean fileCreated = false;
	
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement(tableName);
		doc.appendChild(rootElement);
		
			for (int i = 0; i < tableLoaded.size(); i++) {
				Element staff = doc.createElement("row");
				rootElement.appendChild(staff);
				Attr attr = doc.createAttribute("id");
				attr.setValue(Integer.toString(i));
				staff.setAttributeNode(attr);
				for (int j = 0; j < colsName.length; j++) {
					System.out.println(colsName[j]);
					Element col = doc.createElement(colsName[j].toLowerCase());
					col.appendChild(doc.createTextNode(tableLoaded.get(i)[j]));
					staff.appendChild(col);
				}
			}
		
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(savingPath));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		try {
			transformer.transform(source, result);
			fileCreated = true;
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       return fileCreated;
	}

}
