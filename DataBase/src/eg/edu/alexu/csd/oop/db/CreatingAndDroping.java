package eg.edu.alexu.csd.oop.db;

import java.io.File;

public class CreatingAndDroping {

	public boolean createDatabase(String name) {
		boolean dbCreated = false;
		SavingPath returnPath = new SavingPath();
		String path = returnPath.savingPath(name);
		File file = new File(path);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
				//dbCreated = true;
			} else {
				System.out.println("Failed to create directory!");
				//dbCreated = false;
			}
		}
		return true;
	}

	public boolean dropDatabase(String name, boolean isNewInstance) {
		boolean dbDroped = false;
		SavingPath returnPath = new SavingPath();
		File file = new File(returnPath.savingPath(name));
		if (file.isDirectory()) {
			dbDroped = true;
			for (File sub : file.listFiles()) {
				sub.delete();
			}
		}
		if (isNewInstance)
			file.delete();
		return dbDroped;
	}

	public boolean dropTable(String tableName, String path) {
		boolean fileDroped = false;
		try {
			File file = new File(path);

			if (file.delete()) {
				fileDroped = true;
			}

		} catch (Exception e) {

			e.printStackTrace();

		}
		return fileDroped;
	}
}