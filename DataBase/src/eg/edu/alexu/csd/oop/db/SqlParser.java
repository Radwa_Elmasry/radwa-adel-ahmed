package eg.edu.alexu.csd.oop.db;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlParser {

	private static SqlParser firstInstance = null;

	private SqlParser() {
	}

	public static SqlParser getInstance() {
		if (firstInstance == null) {
			firstInstance = new SqlParser();
		}
		return firstInstance;
	}
	private Object [][] selected;
	private boolean createAndDrop = false;

	private int insertCount = 0;
	private int deleteCount = 0;
	private int updateCount = 0;

	private XmlWriter write = new XmlWriter();
	private XmlModifier modifier = new XmlModifier();
	private String dbName;
	private Map<String, String[]> tablesAttributes = new HashMap<>();
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getDbName() {
		return dbName;
	}
	private SavingPath returnPath = new SavingPath();
	private CreatingAndDroping cd = new CreatingAndDroping();
	private ArrayList<String[]> dummy = new ArrayList<String[]>();
	public boolean sqlParserCreate(String query) {
		boolean valid = false;
		query = query.replaceAll("\\s+$", "");
		if (query.matches("(?i)^CREATE\\s+?DATABASE\\s+?(\\w+)$")) {
			createAndDrop = false;
			Pattern p = Pattern.compile("(?i)^CREATE\\s+?DATABASE\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			dbName = m.group(1);
			System.out.println(dbName);
			createAndDrop= cd.createDatabase(dbName);
			valid = true;
		} else if (query.matches("(?i)^DROP\\s+?DATABASE\\s+?(\\w+)$")) {
			createAndDrop = false;
			Pattern p = Pattern.compile("(?i)^DROP\\s+?DATABASE\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			String dbName2 = m.group(1);
			System.out.println(dbName2);
			createAndDrop = cd.dropDatabase(dbName2 , false);
			valid = true;
		} else if ( query.matches("(?i)^CREATE\\s+?TABLE\\s+?(\\w+)\\s*\\((.*?)\\)$")) {
			createAndDrop = false;
			Pattern p = Pattern.compile("(?i)^CREATE\\s+?TABLE\\s+?(\\w+)\\s*\\((.*?)\\)$");
			Matcher m = p.matcher(query);
			m.find();
			System.out.println(m.group(0));
			System.out.println(m.group(1));
			System.out.println(m.group(2));
			String tableName = m.group(1);
			String[] column = m.group(2).split("\\,\\s?");
			for (int i = 0; i < column.length; i++) {
				System.out.println(column[i]);
			}
			String[] columnNames = new String[column.length];
			String[] columnTypes = new String[column.length];
			Pattern p1 = Pattern.compile("^(.*?)\\s+?(\\w+)\\s*?$");
			for (int i = 0; i < column.length; i++) {
				Matcher m1 = p1.matcher(column[i]);
				m1.find();
				columnNames[i] = m1.group(1);
				columnTypes[i] = m1.group(2);
			}
			tableName = tableName.toLowerCase();
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			if(returnPath.CheckFileExistence(path)){
				createAndDrop = false;
			}else{
			try{
				createAndDrop = write.writeXml(columnNames, tableName, path , dummy);
		} catch (Exception e) {
			return false;
		}
			}
			tablesAttributes.put(tableName, columnNames);
			valid = true;
		} else if (query.matches("(?i)^DROP\\s+?TABLE\\s+?(\\w+)$")) {
			createAndDrop = false;
			Pattern p = Pattern.compile("(?i)^DROP\\s+?TABLE\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			System.out.println(tableName);
			tableName = tableName.toLowerCase();
			String path = returnPath.savingPath(dbName + File.separator+ tableName + ".xml");
			createAndDrop = cd.dropTable(tableName, path);
			valid = true;
		}

		return valid;
	}


	public boolean iscreateAndDrop() {
		return createAndDrop;
	}

	public void setcreateAndDrop(boolean createAndDrop) {
		this.createAndDrop = createAndDrop;
	}


	public void clearSelected(){
		for (int index = 0;selected != null && index < selected.length; index++) {
		    for (int inner = 0; inner < selected[index].length; inner++) {
		        selected[index][inner] = null;
		    }
		    selected[index] = null;
		}
		selected = null;
	}

	public boolean sqlParserSelect(String query) {
		boolean valid = false;
		query = query.replaceAll("\\s+$", "");
		if (query.matches("(?i)^SELECT\\s+?\\*\\s+?FROM\\s+?(\\w+)$")) {
			clearSelected();
			Pattern p = Pattern.compile("(?i)^SELECT\\s+?(.*?)\\s+?FROM\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(2);
			System.out.println(tableName);
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");	
			selected = modifier.selectAll(tablesAttributes.get(tableName), path);
			valid = true;
		} else if (query.matches("(?i)^SELECT\\s+?(.*?)\\s+?FROM\\s+?(\\w+)$")) {
			clearSelected();
			Pattern p = Pattern.compile("(?i)^SELECT\\s+?(.*?)\\s+?FROM\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			System.out.println(m.group(0));
			System.out.println(m.group(1));
			System.out.println(m.group(2));
			String tableName = m.group(2);
			String[] columnNames = m.group(1).split("\\,\\s?");
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");	
			selected = modifier.selectCols(tablesAttributes.get(tableName), path , columnNames);
			valid = true;
		} else if (query.matches("(?i)^SELECT\\s+?\\*\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$")) {
			clearSelected();
			Pattern p = Pattern.compile("(?i)^SELECT\\s+?\\*\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			String value = m.group(2);
			System.out.println(tableName);
			System.out.println(value);
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");	
			selected = modifier.selectAllConditional(tablesAttributes.get(tableName), path , value);
			valid = true;
		} else if (query.matches("(?i)^SELECT\\s+?(.*?)\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$")) {
			clearSelected();
			Pattern p = Pattern.compile("(?i)^SELECT\\s+?(.*?)\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(2);
			System.out.println(tableName);
			System.out.println(m.group(1));
			String[] columnNames = m.group(1).split("\\,\\s?");
			String value = m.group(3);
			System.out.println(value);
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			selected = modifier.selectColsConditional(tablesAttributes.get(tableName), columnNames, path, value);
			valid = true;
		}

		return valid;
	}

	public Object[][] getSelected() {
		return selected;
	}

	public boolean sqlParserUpdate(String query) {

		boolean valid = false;
		query = query.replaceAll("\\s+$", "");
		if (query.matches("(?i)^INSERT\\s+?INTO\\s+?(\\w+)\\s+?VALUES\\s*?\\((.*?)\\)$")) {
			Pattern p = Pattern.compile("(?i)^INSERT\\s+?INTO\\s+?(\\w+)\\s+?VALUES\\s*?\\((.*?)\\)$");
			Matcher m = p.matcher(query);
			m.find();
			System.out.println(m.group(0));
			System.out.println(m.group(1));
			System.out.println(m.group(2));
			String tableName = m.group(1);
			tableName = tableName.toLowerCase();
			String c = m.group(2).replaceAll("\"", "");
			String[] contents = c.split("\\,\\s?");
			for (int i = 0; i < contents.length; i++) {
				contents[i] = contents[i].replaceAll("'", "");
			}
			 
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			for (int i = 0; i < tablesAttributes.get(tableName).length; i++) {
				System.out.println(tablesAttributes.get(tableName)[i]);
			}
			try{
			write.writeXml(tablesAttributes.get(tableName), tableName , path ,modifier.insert(tablesAttributes.get(tableName), contents, path));
		} catch (Exception e) {
			return false;
		}
			insertCount = 1;
			valid = true;
		} else if (query.matches("(?i)^INSERT\\s+?INTO\\s+?(\\w+)\\s*?\\((.*?)\\)\\s*?VALUES\\s*?\\((.*?)\\)$")) {
			Pattern p = Pattern.compile("(?i)^INSERT\\s+?INTO\\s+?(\\w+)\\s*?\\((.*?)\\)\\s*?VALUES\\s*?\\((.*?)\\)$");
			Matcher m = p.matcher(query);
			m.find();
			System.out.println(m.group(0));
			System.out.println(m.group(1));
			System.out.println(m.group(2));
			System.out.println(m.group(3));
			String tableName = m.group(1);
			tableName = tableName.toLowerCase();
			String[] columns = m.group(2).split("\\,\\s?");
			String c = m.group(3).replaceAll("\"", "");
			String[] contents = c.split("\\,\\s?");
			for (int i = 0; i < contents.length; i++) {
				contents[i] = contents[i].replaceAll("'", "");
			}
			String[] schema =  tablesAttributes.get(tableName);
			for (int i = 0; i < schema.length; i++) {
				schema[i] = schema[i].toLowerCase();
				for (int j = 0; j < columns.length; j++) {
					columns[j] = columns[j].toLowerCase();
					if(schema[i].equals(columns[j])){
						String tempS = contents[i];
						contents[i] = contents[j];
						contents[j] = tempS;
						String tempS2 = columns[i];
						columns[i] = columns[j];
						columns[j] = tempS2;
						break;
					}
				}
			}
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");	
			try{
			write.writeXml(schema, tableName, path, modifier.insert(schema, contents, path));
		} catch (Exception e) {
			return false;
		}
			insertCount = 1;
			valid = true;
		} else if (query.matches("(?i)^DELETE\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$")) {
			Pattern p = Pattern.compile("(?i)^DELETE\\s+?FROM\\s+?(\\w+)\\s+?WHERE\\s+?(.*?)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			tableName = tableName.toLowerCase();
			System.out.println(tableName);
			String s = m.group(2);
			s = s.replaceAll("'", "");
			System.out.println(s);
			
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			try{
			write.writeXml(tablesAttributes.get(tableName), tableName, path, modifier.delete(tablesAttributes.get(tableName), path, s));
		} catch (Exception e) {
			return false;
		}
		    deleteCount = modifier.getCountDelete();
			valid = true;
		} else if (query.matches("(?i)^DELETE\\s+?FROM\\s+?(\\w+)$")) {
			Pattern p = Pattern.compile("(?i)^DELETE\\s+?FROM\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			tableName = tableName.toLowerCase();
			System.out.println(tableName);
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			try{
			write.writeXml(tablesAttributes.get(tableName), tableName, path, modifier.deleteAll(tablesAttributes.get(tableName), path));
		} catch (Exception e) {
			return false;
		}
			  deleteCount = modifier.getCountDelete();
			valid = true;
		} else if (query.matches("(?i)^DELETE\\s+?\\*\\s+?FROM\\s+?(\\w+)$")) {
			Pattern p = Pattern.compile("(?i)^DELETE\\s+?\\*\\s+?FROM\\s+?(\\w+)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			System.out.println(tableName);
			tableName = tableName.toLowerCase();
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			try{
				write.writeXml(tablesAttributes.get(tableName), tableName, path, modifier.deleteAll(tablesAttributes.get(tableName), path));
			} catch (Exception e) {
				return false;
			}
				  deleteCount = modifier.getCountDelete();
			valid = true;
		} else if (query.matches("(?i)^UPDATE\\s+?(\\w+)\\s+?SET\\s+?(.*?)\\s+?WHERE\\s+?(.*?)$")) {
			Pattern p = Pattern.compile("(?i)^UPDATE\\s+?(\\w+)\\s+?SET\\s+?(.*?)\\s+?WHERE\\s+?(.*?)$");
			Matcher m = p.matcher(query);
			m.find();
			String tableName = m.group(1);
			tableName = tableName.toLowerCase();
			System.out.println(tableName);
			System.out.println(m.group(2));
			String[] columns = m.group(2).split("\\,\\s?");
			String value = m.group(3);
			value = value.replaceAll("'", "");
			System.out.println(value);
			String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			try{
			write.writeXml(tablesAttributes.get(tableName), tableName, path, modifier.update(tablesAttributes.get(tableName), path, columns, value));
		} catch (Exception e) {
			return false;
		}
			updateCount = modifier.getCountUpdate();
			valid = true;
		}else if (query.matches("(?i)^UPDATE\\s+?(\\w+)\\s+?SET\\s+?(.*?)$")) {
			   Pattern p = Pattern.compile("(?i)^UPDATE\\s+?(\\w+)\\s+?SET\\s+?(.*?)$");
			   Matcher m = p.matcher(query);
			   m.find();
			   String tableName = m.group(1);
			   tableName = tableName.toLowerCase();
			   System.out.println(tableName);
			   System.out.println(m.group(2));
			   String[] columns = m.group(2).split("\\,\\s?");
			   String path = returnPath.savingPath(dbName + File.separator + tableName + ".xml");
			   try {
				   write.writeXml(tablesAttributes.get(tableName), tableName, path, modifier.updateWithoutWhere(tablesAttributes.get(tableName), path, columns));
			} catch (Exception e) {
				return false;
			}
			   updateCount = modifier.getCountUpdate();
			   valid = true;
			  }

		return valid;
	}

	public int getInsertCount() {
		return insertCount;
	}

	public void setInsertCount(int insertCount) {
		this.insertCount = insertCount;
	}

	public int getDeleteCount() {
		return deleteCount;
	}

	public void setDeleteCount(int deleteCount) {
		this.deleteCount = deleteCount;
	}

	public int getUpdateCount() {
		return updateCount;
	}

	public void setUpdateCount(int updateCount) {
		this.updateCount = updateCount;
	}

}
