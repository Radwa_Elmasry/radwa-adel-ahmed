package eg.edu.alexu.csd.oop.db;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import eg.edu.alexu.csd.oop.db.Database;

public class DbTest {

	@Test
	public void test() throws SQLException {
		Database db1 = new DatabaseEngine();
		db1.createDatabase("sample",true);
		assertTrue(db1.executeStructureQuery("CREATE TABLE table_name1(column_name1 varchar, column_name2 int, column_name3 varchar)"));
		assertTrue(db1.executeStructureQuery("CREATE DATABASE TestDB"));
		assertTrue(db1.executeStructureQuery("CREATE TABLE table_name1(column_name1 varchar, column_name2 int, column_name3 varchar)"));
		Database db2 = new DatabaseEngine();
		assertTrue(db2.executeStructureQuery("CREATE DATABASE TestDB"));
		assertTrue(db2.executeStructureQuery("DROP DATABASE TestDB"));
		assertTrue(db2.executeStructureQuery("CREATE TABLE table_name1(column_name1 varchar, column_name2 int, column_name3 varchar)"));
		assertTrue(db2.executeStructureQuery("CREATE DATABASE TestDB"));
		assertTrue(db2.executeStructureQuery("CREATE TABLE table_name2(column_name1 varchar, column_name2 int, column_name3 varchar)"));

	}

}

