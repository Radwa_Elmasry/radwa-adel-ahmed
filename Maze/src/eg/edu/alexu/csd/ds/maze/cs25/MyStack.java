package eg.edu.alexu.csd.ds.maze.cs25;

public class MyStack implements QandS{

    MyLinkedList list = new MyList();
		
	public Object remove() throws RuntimeException{
		// TODO Auto-generated method stub
		if(isEmpty()){
			throw new RuntimeException("Stack is empty");
		}
		Object temp = list.get(0);
		list.remove(0);
		return temp;
	}

	public void add (Object element) {
		// TODO Auto-generated method stub
		list.add(0, element);
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (list.size() == 0)
				return true;
		else
			return false;
	}

	

}
