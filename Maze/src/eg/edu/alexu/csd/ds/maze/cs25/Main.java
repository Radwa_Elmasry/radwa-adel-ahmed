package eg.edu.alexu.csd.ds.maze.cs25;

import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	
	private static Scanner sc;
	private static File maze;
	private static ReadFromFile r;

	public static void main(String[] args) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException{
		// TODO Auto-generated method stub
		
		File file = new File("map.txt");
		System.out.println("The Maze");
		 maze = new File("map.txt");
		 r = new ReadFromFile(file);
		 Point s = ReadFromFile.start;
		 Point e = ReadFromFile.end;
	  
		  sc = new Scanner(System.in);
		  
		  System.out.println("Press (dfs) for DFS");
		  System.out.println("Press (bfs) for BFS");
		  String choose = sc.nextLine();
		  
		  if(choose.equals("dfs")){
			  System.out.println("The DFS path is :");
			  Class c = Class.forName("eg.edu.alexu.csd.ds.maze.cs25.DFS");
			  DFS pathD = (DFS) c.newInstance();
			  pathD.dfs(s , e);  
		  }
		  else if (choose.equals("bfs")){
			  System.out.println("The BFS path is :");
			  Class c = Class.forName("eg.edu.alexu.csd.ds.maze.cs25.BFS");
			  BFS pathB = (BFS) c.newInstance();
			  pathB.bfs(s, e);
		  }
		  
		  autoMaze g = new autoMaze();
	      g.generateMap();
	
	}

}
