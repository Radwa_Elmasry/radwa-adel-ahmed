package eg.edu.alexu.csd.ds.maze.cs25;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {
	
	public static boolean map [][];
	public static Point start;
	public static Point end;
	public static int HEIGHT , WIDTH;
	private static BufferedReader buffer;
	

	public ReadFromFile(File map) {
		try {
			readFile(map);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean[][] readFile(File file) throws IOException {

		buffer = new BufferedReader(new FileReader(file));

		String dimensions = new String();

		dimensions = buffer.readLine();

		String[] s = dimensions.split(" ");
	    HEIGHT = Integer.parseInt(s[0]);
	    WIDTH = Integer.parseInt(s[1]);
		/*System.out.println(HEIGHT);
		System.out.println(WIDTH);*/
		char[][] maze = new char[WIDTH][HEIGHT];

	            String line;
	            int col = 0, row = 0;
	            while((line = buffer.readLine()) != null && row < HEIGHT) {
	                for(col = 0; col < line.length() && col < WIDTH; col++) {
	                    maze[col][row] = line.charAt(col);
	                }
	                row++;
	            }
	
		for (int x = 0; x < HEIGHT; x++) {
			for (int y = 0; y < WIDTH; y++) {
				System.out.print(maze[y][x]);
			}
			System.out.println();
		}
		
	    map = new boolean[HEIGHT][WIDTH];
		
	    start = new Point();
		end   = new Point();
		for(int r = 0 ; r < HEIGHT ; r++){
			for(int a = 0 ; a < WIDTH ; a++){
				
				if(maze[a][r] == '.'){
					map[r][a] = true;
				}else if (maze[a][r] == '#'){
					map[r][a] = false;
				}else if(maze[a][r] == 'S'){
					map[r][a] = true;
					start.x = r;
					start.y = a;
				}else if(maze[a][r] == 'E'){
					map[r][a] = true;
					end.x = r;
					end.y = a;
				}
				
			}
		}
			/*for (int x = 0; x < HEIGHT; x++) {
				for (int y = 0; y < WIDTH; y++) {
					System.out.print(map[x][y]);
				}
				System.out.println();
			}
			
			System.out.println(start.x);
			System.out.println(start.y);
			System.out.println(end.x);
			System.out.println(end.y);*/
			return map;
		}
		

	}


