package eg.edu.alexu.csd.ds.maze.cs25;

import java.awt.Point;
import java.io.File;

public class BFS {
	static boolean[][] marked = ReadFromFile.map;;

	QandS q = new QueueLinkedList();
	File file = new File("map.txt");
	//ReadFromFile read = new ReadFromFile(file);
	Point first = new Point();
	static int row = ReadFromFile.HEIGHT;
	static int col = ReadFromFile.WIDTH;
	Point[][] check = new Point[row][col];
	int x,y;
	public void bfs(Point s, Point e) {
		/*
		 * for (int x = 0; x < readFromFile.HEIGHT; x++) { for (int y = 0; y <
		 * readFromFile.WIDTH; y++) { System.out.print([x][y]); }
		 * System.out.println(); }
		 */
		MyLinkedList list = new MyList();
		QandS stack = new MyStack();
		Point here = new Point();
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				check[i][j] = new Point();
			}
		}
		marked[s.x][s.y] = false;
		q.add(s);
		list.add(s);
		while (!q.isEmpty()) {
			first = (Point) q.remove();

			if (first.x == e.x) {
				if (first.y - 1 == e.y) {
					marked[first.x][first.y - 1] = false;
					check[e.x][e.y] = first;
					break;
				} else if (first.y + 1 == e.y) {
					marked[first.x][first.y + 1] = false;
					check[e.x][e.y] = first;
					break;
				}
			} else if (first.y == e.y) {
				if (first.x - 1 == e.x) {
					marked[first.x - 1][first.y] = false;
					check[e.x][e.y] = first;
					break;
				} else if (first.x + 1 == e.x) {
					marked[first.x + 1][first.y] = false;
					check[e.x][e.y] = first;
					break;
				}
			}
			/*
			 * if (first.x == e.x && first.y == e.y) { marked[e.x][e.y + 1] =
			 * false; check[e.x][e.y] = first; break;
			 * 
			 * }
			 */
			if (valid(first.x + 1, first.y) && marked[first.x + 1][first.y]) {
				marked[first.x + 1][first.y] = false;
				Point next = new Point(first.x + 1, first.y);
				q.add(next);
				check[first.x + 1][first.y] = first;
			}
			if (valid(first.x, first.y + 1) && marked[first.x][first.y + 1]) {
				marked[first.x][first.y + 1] = false;
				Point next = new Point(first.x, first.y + 1);
				q.add(next);
				check[first.x][first.y + 1] = first;
			}
			if (valid(first.x - 1, first.y) && marked[first.x - 1][first.y]) {
				marked[first.x - 1][first.y] = false;
				Point next = new Point(first.x - 1, first.y);
				q.add(next);
				check[first.x - 1][first.y] = first;
			}
			if (valid(first.x, first.y - 1) && marked[first.x][first.y - 1]) {
				marked[first.x][first.y - 1] = false;
				Point next = new Point(first.x, first.y - 1);
				q.add(next);
				check[first.x][first.y - 1] = first;
			}
		}
		if(marked[e.x][e.y]==true)
		{
			System.out.println("No");
		}
		else
		{
			stack.add(e);
			x=e.x;
			y=e.y;
			here=new Point (x,y);
			while(here.x != s.x || here.y != s.y)
			{
				x=check[here.x][here.y].x;
				y=check[here.x][here.y].y;
				here= new Point (x,y);
				stack.add(here);
			}
			
			
		}
		Point[] arr = new Point[row * col];
		int count = 0;
		for (int a = 0; a < arr.length; a++) {

			arr[a] = new Point();
		}
		Point print = new Point();
		while(!stack.isEmpty())
		{
			print=(Point) stack.remove();
			System.out.println("(" + print.x + "," + print.y + ")");
			arr[count] = print;
			count++;
		}
		/*for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				System.out.println(check[i][j]);
			}
		}*/

	char[][] grid = new char[row][col];
	
	for (int p = 0; p < row; p++) {
		for (int q = 0; q < col; q++) {
			grid[p][q] = '-';
		}
	}

	for (int u = 0; u < row; u++) {
		for (int v = 0; v < col; v++) {
			for (int j = 0; j < count; j++) {
				if (arr[j].x == u && arr[j].y == v) {
					grid[u][v] = 'X';
					if (arr[j].x == e.x && arr[j].y == e.y) {
						break;
					}
				}
			}
		}
	}
	for (int a = 0; a < row; a++) {
		for (int r = 0; r < col; r++) {
			System.out.print(grid[a][r] + " ");
		}
		System.out.println();
	}
	}

	/*
	 * Point p = new Point(); Point q = new Point(); for (int i = 1; i <
	 * list.size(); i++) { p = (Point) list.get(i); q = (Point) list.get(i - 1);
	 * if (q.x - p.x >= 1 && q.y - p.y >= 1) { list.remove(i); i--; }
	 * 
	 * } Point a = new Point(); for (int j = 0; j < list.size(); j++) { a =
	 * (Point) list.get(j);
	 * 
	 * System.out.println("(" + a.x + "," + a.y + ")");
	 * 
	 * }
	 * 
	 * }
	 */

	public boolean valid(int x, int y) {
		if ((y >= 0 && y < marked[0].length) && (x >= 0 && x < marked.length)) {
			return true;

		}
		return false;
	}
	
}

