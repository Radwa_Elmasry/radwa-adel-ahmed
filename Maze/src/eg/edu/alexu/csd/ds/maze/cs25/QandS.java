package eg.edu.alexu.csd.ds.maze.cs25;

public interface QandS {
	
	 public void add(Object element);
	 
	 public Object remove();
	 
	 public boolean isEmpty();


}
