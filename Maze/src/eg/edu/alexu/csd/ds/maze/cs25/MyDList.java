package eg.edu.alexu.csd.ds.maze.cs25;

public class MyDList implements MyLinkedList {

	private DListNode header = null;
	private DListNode trailer = null;
	int Dsize = 0;

	@Override
	public void add(int index, Object element) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub

		DListNode newNode = new DListNode(element,null,null);
		if (index <= Dsize && index >= 0) {
			if (index == 0) {
				if (isEmpty()) {
					trailer = newNode;
					newNode.next = getHeader();
				    setHeader(newNode);
					
				} else {
					getHeader().previous = newNode;
					newNode.next = getHeader();
					setHeader(newNode);  
				}
				Dsize++;

			}
			else{

				DListNode i = getHeader();
				for (int j = 0; j < index - 1; j++) {

					i = i.next;
				}
				newNode.next = i.next;
				i.next = newNode;
				if(index==Dsize)trailer=newNode;
				Dsize++;
			}
		} else {
			throw new IndexOutOfBoundsException();
		}

		}

	@Override
	public void add(Object element) {
		// TODO Auto-generated method stub

		DListNode newNode = new DListNode(element,null,null);
		if (isEmpty()) {

			setHeader(newNode);
			trailer = newNode;
		} else {
			DListNode i = trailer;
			newNode.previous = i;
			i.next = newNode;
			trailer = newNode;
		}
		Dsize++;
	}

	@Override
	public Object get(int index) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		if (index <= Dsize && index >= 0) {
			DListNode i = getHeader();
			for (int j = 0; j < index; j++) {

				i = i.next;
			}
			return i.value;
		} else {
			throw new IndexOutOfBoundsException();
		}

	}

	@Override
	public void set(int index, Object element) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		
		if (index <= Dsize && index >= 0) {
			if (index == 0) {
				getHeader().value = element;
			} else {
				DListNode i = getHeader();
				for (int j = 0; j < index; j++) {
					i = i.next;
				}
				i.value = element;
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
	
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		setHeader(trailer = null);
		Dsize = 0;

	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (getHeader() == null || Dsize == 0);
	}

	@Override
	public void remove(int index) throws IndexOutOfBoundsException {
		// TODO Auto-generated method stub
		
		if (index < Dsize && index >= 0 && getHeader() != null) {
			if (index == 0) {
				setHeader(getHeader().next);
				Dsize--;
			} else {
				DListNode i = getHeader();
				for (int j = 0; j < index - 1; j++) {
					i = i.next;
				}
				DListNode j = i.next;
				i.next = j.next;
				Dsize--;
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
		
		

	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return Dsize;
	}

	@Override
	public MyLinkedList sublist(int fromIndex, int toIndex)
	// TODO Auto-generated method stub
			throws IndexOutOfBoundsException {
		MyLinkedList newList = new MyDList();
		if (getHeader() == null) {
			return newList;
		}
		if (fromIndex < 0 || fromIndex > Dsize - 1 || toIndex < 0
				|| toIndex > Dsize - 1 || fromIndex > toIndex) {
			throw new IndexOutOfBoundsException();
		}
		DListNode i = getHeader();
		int j = 0;
		for (int counter = 0; counter < fromIndex; counter++) {
			i = i.next;
		}

		for (int counter = fromIndex; counter <= toIndex; counter++) {
			newList.add(j, i.value);
			i = i.next;
			j++;
		}
		return newList;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		DListNode i = getHeader();
		if (getHeader() == null) {
			return false;
		}
		while (i != null) {

			if (i.value == o) {
				return true;
			}
			i = i.next;
		}
		return false;
	}

	public DListNode getHeader() {
		return header;
	}

	public void setHeader(DListNode header) {
		this.header = header;
	}

}
