package eg.edu.alexu.csd.ds.maze.cs25;

import java.awt.Point;

public class DFS {

	static QandS stack = new MyStack();

	static boolean visited[][] = ReadFromFile.map;
	static int row = ReadFromFile.HEIGHT;
	static int col = ReadFromFile.WIDTH;
	static Point top;
	static Point left, right, up, down;

	Point parent[][] = new Point[100][100];
	Point[] pathDfs = new Point[100];

	public void dfs(Point s, Point e) {

		for (int k = 0; k < row; k++) {
			for (int j = 0; j < col; j++) {

				parent[k][j] = new Point();
			}
		}

		stack.add(s);
		while (!stack.isEmpty()) {

			top = (Point) stack.remove();

			if (visited[top.x][top.y] == true) {

				if (top.x == e.x && top.y == e.y) {
					Point way = new Point();
					way.x = top.x;
					way.y = top.y;
					parent[top.x][top.y] = way;
					break;
				}

				visited[top.x][top.y] = false;

				if (top.x - 1 >= 0 && visited[top.x - 1][top.y] == true) {
					up = new Point(top.x - 1, top.y);
					stack.add(up);
					Point Way = new Point();
					Way.x = top.x;
					Way.y = top.y;
					parent[top.x - 1][top.y] = Way;
				}

				if (top.x + 1 >= 0 && top.x + 1 < row
						&& visited[top.x + 1][top.y] == true) {
					down = new Point(top.x + 1, top.y);
					stack.add(down);
					Point Way = new Point();
					Way.x = top.x;
					Way.y = top.y;
					parent[top.x + 1][top.y] = Way;

				}

				if (top.y + 1 >= 0 && top.y + 1 < col
						&& visited[top.x][top.y + 1] == true) {
					left = new Point(top.x, top.y + 1);
					stack.add(left);
					Point Way = new Point();
					Way.x = top.x;
					Way.y = top.y;
					parent[top.x][top.y + 1] = Way;
				}
				if (top.y - 1 >= 0 && top.y - 1 < col
						&& visited[top.x][top.y - 1] == true) {
					right = new Point(top.x, top.y - 1);
					stack.add(right);
					Point Way = new Point();
					Way.x = top.x;
					Way.y = top.y;
					parent[top.x][top.y - 1] = Way;
				}

			}

		}
		
		for(int y = 0 ; y < 100 ; y++){
			pathDfs[y] = new Point();
		}	
		char[][] a=new char[5][5];
		int index = 0;
		int i = top.x, j = top.y;
		while (i != s.x || j != s.y) {
			Point temp = (Point) parent[i][j];
			//pathDfs[index++] = temp;
			a[temp.x][temp.y]='0';
			i = temp.x;
			j = temp.y;

		}
		for(int k=0;k<5;k++){for(int l=0;l<5;l++)System.out.print(a[k][l]);System.out.println("");}
		Point[] arr = new Point[row * col];
		int count = 0;
		for (int p = 0; p < arr.length; p++) {

			arr[p] = new Point();
		}

		char[][] grid = new char[row][col];

		for (int r = 0; r < row * col; r++) {
			if (pathDfs[r].x == e.x && pathDfs[r].y == e.y) {
				System.out.println("(" + pathDfs[r].x + "," + pathDfs[r].y
						+ ")");
				arr[count] = pathDfs[r];
				count++;
				break;
			} else {
				System.out.println("(" + pathDfs[r].x + "," + pathDfs[r].y
						+ ")");
				arr[count] = pathDfs[r];
				count++;
			}
		}

		for (int p = 0; p < row; p++) {
			for (int q = 0; q < col; q++) {
				grid[p][q] = '-';
			}
		}

		for (int u = 0; u < row; u++) {
			for (int v = 0; v < col; v++) {
				for (int m = 0; m < count; m++) {
					if (arr[m].x == u && arr[m].y == v) {
						grid[u][v] = 'X';
						if (arr[m].x == e.x && arr[m].y == e.y) {
							break;
						}
					}
				}
			}
		}
		for (int b = 0; b < row; b++) {
			for (int r = 0; r < col; r++) {
				System.out.print(grid[b][r] + " ");
			}
			System.out.println();
		}

	}
}