package eg.edu.alexu.csd.ds.maze.cs25;

public class MyList implements MyLinkedList {

	private ListNode head = null;
	private int size = 0;
	public void print() {
		  // TODO Auto-generated method stub
		  ListNode i = getHead();
		  while (i!= null) {
		   System.out.print(i.value);
		   i = i.next;
		  }
		 }
	public void add(int index, Object element) throws IndexOutOfBoundsException {
		ListNode newNode = new ListNode(element);
		if (index <= getSize() && index >= 0) {
			if (index == 0) {
				newNode.next = getHead();
				setHead(newNode);
				setSize(getSize() + 1);
			} else {
				ListNode i = getHead();
				for (int j = 0; j < index - 1; j++) {
					i = i.next;
				}
				newNode.next = i.next;
				i.next = newNode;
				setSize(getSize() + 1);
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public void add(Object element) {
		ListNode newNode = new ListNode(element);
		newNode.next = null;
		ListNode i = getHead();
		if (getHead() != null && getSize() != 0) {
			while (i.next != null) {
				i = i.next;
			}
			i.next = newNode;
		} else {
			setHead(newNode);
		}
		setSize(getSize() + 1);
	}

	public Object get(int index) throws IndexOutOfBoundsException {

		if (index <= getSize() && index >= 0) {
			ListNode i = getHead();
			for (int j = 0; j < index; j++) {

				i = i.next;
			}
			return i.value;
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public void set(int index, Object element) throws IndexOutOfBoundsException {

		if (index <= getSize() && index >= 0) {
			if (index == 0) {
				getHead().value = element;
			} else {
				ListNode i = getHead();
				for (int j = 0; j < index; j++) {
					i = i.next;
				}
				i.value = element;
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public void clear() {

		setHead(null);
		setSize(0);
	}

	public boolean isEmpty() {

		if (getHead() == null && getSize() == 0)
			return true;
		else
			return false;
	}

	public void remove(int index) throws IndexOutOfBoundsException {
		if (index < getSize() && index >= 0 && getHead() != null) {
			if (index == 0) {
				setHead(getHead().next);
				setSize(getSize() - 1);
			} else {
				ListNode i = getHead();
				for (int j = 0; j < index - 1; j++) {
					i = i.next;
				}
				ListNode j = i.next;
				i.next = j.next;
				setSize(getSize() - 1);
			}
		} else {
			throw new IndexOutOfBoundsException();
		}
	}

	public int size() {

		return getSize();

	}

	public MyLinkedList sublist(int fromIndex, int toIndex)
			throws IndexOutOfBoundsException {
		MyLinkedList newList = new MyList();
		if (getHead() == null) {
			return newList;
		}
		if (fromIndex < 0 || fromIndex > getSize() - 1 || toIndex < 0
				|| toIndex > getSize() - 1 || fromIndex > toIndex) {
			throw new IndexOutOfBoundsException();
		}
		ListNode i = getHead();
		int j = 0;
		for (int counter = 0; counter < fromIndex; counter++) {
			i = i.next;
		}

		for (int counter = fromIndex; counter <= toIndex; counter++) {
			newList.add(j, i.value);
			i = i.next;
			j++;
		}
		return newList;
	}

	public boolean contains(Object o) {
		ListNode i = getHead();
		if (getHead() == null) {
			return false;
		}
		while (i != null) {

			if (i.value == o) {
				return true;
			}
			i = i.next;
		}
		return false;
	}

	public ListNode getHead() {
		return head;
	}

	public void setHead(ListNode head) {
		this.head = head;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
