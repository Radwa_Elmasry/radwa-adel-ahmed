package eg.edu.alexu.csd.ds.maze.cs25;

public class DListNode {

	public Object value;
	public DListNode next;
	public DListNode previous;

	public DListNode(Object element, DListNode n , DListNode p) {
		// TODO Auto-generated constructor stub
		this.value = element;
		this.next = n;
		this.previous = p;
		
	}

}
