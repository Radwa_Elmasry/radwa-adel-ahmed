package eg.edu.alexu.csd.ds.maze.cs25;

import java.awt.Point;
import java.util.Random;

public class autoMaze {
	Random rn = new Random();
	int height = rn.nextInt(16) + 5;
	int width = rn.nextInt(16) + 5;
	int x = rn.nextInt(height) + 0;
	int y = rn.nextInt(width) + 0;
	int x2 = rn.nextInt(height) + 0;
	int y2 = rn.nextInt(width) + 0;
	Point start = new Point(x, y);
	Point end = new Point(x2, y2);
	char[][] array = new char[height][width];

	public void generateMap() {
		while (Math.abs(end.x - start.x) <= 3 || Math.abs(end.y - start.y) <= 3) {
			int x2 = rn.nextInt(height);
			int y2 = rn.nextInt(width);
			end = new Point(x2, y2);
		}

		int counter = 0;
		for (int k = 0; k < height; k++) {
			for (int w = 0; w < width; w++) {
				array[k][w] = '#';
			}
		}

		if (start.x < end.x) {
			for (int w = start.x; w < end.x; w++) {
				array[w][start.y] = '.';
				counter++;
			}
		} else {
			for (int w = end.x; w < start.x; w++) {
				array[w][start.y] = '.';
				counter++;
			}
		}
		if (start.y > end.y) {
			for (int w = end.y; w < start.y; w++) {
				array[end.x][w] = '.';
				counter++;
			}
		} else {
			for (int w = start.y; w < end.y; w++) {
				array[end.x][w] = '.';
				counter++;
			}
		}
		while (counter < (height * width / 3)) {
			int dotX = rn.nextInt(height) + 0;
			int dotY = rn.nextInt(width) + 0;
			array[dotX][dotY] = '.';
			counter++;
		}
		array[start.x][start.y] = 'S';
		array[end.x][end.y] = 'E';
		System.out.println(height + " " + width);
		for (int k = 0; k < height; k++) {
			for (int w = 0; w < width; w++) {
				System.out.print(array[k][w]);
			}
			System.out.println();
		}

	}
}