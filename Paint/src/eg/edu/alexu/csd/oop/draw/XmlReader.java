package eg.edu.alexu.csd.oop.draw;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
public class XmlReader {

	public ArrayList<Shape> readXml(String savingPath) {
		// savingPath = "/debug/radwa.xml";
		ArrayList<Shape> shapes = new ArrayList<Shape>(0);
		try {
			Shape sh = null;
			File fXmlFile = new File(savingPath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getDocumentElement().getChildNodes();
            System.out.println(nList.getLength());
			for (int i = 0; i < nList.getLength(); i++) {

				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String classLoaded = eElement.getAttribute("id");		
					
						sh = new NewShape();
						sh = (Shape) Class.forName(classLoaded).newInstance();
	                    System.out.println(sh);
					String x = eElement.getElementsByTagName("XPosition").item(0).getTextContent();
					String y = eElement.getElementsByTagName("YPosition").item(0).getTextContent();
					if (!x.equals("null") && !y.equals("null")) {
					
						((Shape) sh).setPosition(new Point(Integer.parseInt(x), Integer.parseInt(y)));
					
					}
					String in = eElement.getElementsByTagName("InColor").item(0).getTextContent();
					if (!in.equals("null")) {
						int color = Integer.parseInt(in); 
						((Shape) sh).setColor(new Color(color));
							
					}
					String out = eElement.getElementsByTagName("OutColor").item(0).getTextContent();
					if (!out.equals("null")) {
						
						int color =Integer.parseInt(out); 
						((Shape) sh).setColor(new Color(color));
						
					}
					String found = eElement.getElementsByTagName("findMap").item(0).getTextContent();
					System.out.println(found);
					if (found.equals("true")) {
						Map<String, Double> propertiesLoaded = new HashMap<>();
						Set<String> keys = ((Shape) sh).getProperties().keySet();
						for (String string : keys) {
 							String str = eElement.getElementsByTagName(string).item(0).getTextContent();
							if (!str.equals("null")) {
								propertiesLoaded.put(string, Double.valueOf(str));
							} else {
								propertiesLoaded.put(string, null);
							}
						}
						((Shape) sh).setProperties(propertiesLoaded);
					}

				}
				shapes.add(((Shape) sh));
 			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return shapes;
	}

}
