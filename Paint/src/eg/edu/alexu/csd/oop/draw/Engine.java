package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;

import java.util.ArrayList;

import java.util.List;

public class Engine implements DrawingEngine {
	private ArrayList<Shape> shapes = new ArrayList<Shape>();
	private History history = History.getInstance();
	private static Engine firstInstance = null;
	private boolean loaded = false;

	private Engine() {
		History.getInstance().destoryInstance();
	}

	@Override
	public void refresh(Graphics canvas) {

		for (Shape s : shapes) {
			s.draw(canvas);
		}
	}

	@Override
	public void addShape(Shape shape) {
		shapes.add(shape);
		history.track(shapes);
		loaded = false;
	}

	@Override
	public void removeShape(Shape shape) {
		boolean flag = false;

		for (int i = 0; i < shapes.size(); i++) {
			if (shapes.get(i).equals(shape)) {
				shapes.remove(i);

				flag = true;
			}

		}
		history.track(shapes);
		System.out.println(shapes + "remove");
		if (!flag) {
			throw new RuntimeException();
		}

	}

	@Override
	public void updateShape(Shape oldShape, Shape newShape) {
		boolean flag = false;

		shapes.remove(oldShape);
		shapes.add(newShape);
		flag = true;

		history.track(shapes);
		if (!flag) {
			throw new RuntimeException();
		}

	}

	@Override
	public Shape[] getShapes() {

		Shape[] array = new Shape[shapes.size()];
		array = shapes.toArray(array);
		return array;
	}

	@Override
	public List<Class< ? extends Shape>> getSupportedShapes() {
		// TODO Auto-generated method stub
		MyClassLoader load = new MyClassLoader();
		return load.loadClasses();

	}

	@Override
	public void undo() {
		try {
			if (!loaded) {
				shapes = new ArrayList<Shape>(history.getPrev());
			}

		} catch (Exception e) {
			shapes.clear();
		}

	}

	@Override
	public void redo() {
		try {
			shapes = new ArrayList<Shape>(history.getNext());
		} catch (Exception e) {
			throw new RuntimeException();
		}

	}

	@Override
	public void save(String path) {
		XmlWriter saveX = new XmlWriter();
		JsonWriter saveJ = new JsonWriter();
		String[] exten = path.split("\\.");
		Shape[] array = getShapes();

		if (exten[exten.length - 1].toLowerCase().equals("xml")) {
			saveX.writeXml(path, array);
		} else if (exten[exten.length - 1].toLowerCase().equals("json")) {
			saveJ.writeJson(path, array);
		}
	}

	@Override
	public void load(String path) {
		XmlReader loadX = new XmlReader();
		JsonReader loadJ = new JsonReader();

		String[] exten = path.split("\\.");
		if (exten[exten.length - 1].toLowerCase().equals("xml")) {
			shapes.clear();
			shapes = loadX.readXml(path);
			history.deleteHistory();
			history.track(shapes);

		} else if (exten[exten.length - 1].toLowerCase().equals("json")) {
			shapes.clear();
			shapes = loadJ.readJson(path);
			history.deleteHistory();
			history.track(shapes);

		}
		loaded = true;
	}

	public static Engine getInstance() {

		if (firstInstance == null) {

			// This is here to test what happens if threads try
			// to create instances of this class
			firstInstance = new Engine();

		}
		return firstInstance;
	}

	public static void destoryInstance() {
		firstInstance = null;
	}

}
