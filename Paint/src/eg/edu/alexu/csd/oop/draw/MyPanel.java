package eg.edu.alexu.csd.oop.draw;

import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private Engine e = Engine.getInstance();
	private static final long serialVersionUID = 1L;

	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		e.refresh(g);

	}
}
