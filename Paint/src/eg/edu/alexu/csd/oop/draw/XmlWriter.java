package eg.edu.alexu.csd.oop.draw;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlWriter {

	public void writeXml(String savingPath, Shape[] shapes) {
		// savingPath = "/debug/radwa.xml";
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = null;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException("35write", e1);
		}

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("SavedShapes");
		doc.appendChild(rootElement);
		for (int i = 0; i < shapes.length; i++) {
			Object temp = shapes[i];
			Element staff = doc.createElement("Shape");
			rootElement.appendChild(staff);

			Attr attr = doc.createAttribute("id");
			attr.setValue(temp.getClass().getName());
		
			staff.setAttributeNode(attr);

			Element xPos = doc.createElement("XPosition");
			if (((Shape) temp).getPosition() != null)
				xPos.appendChild(doc.createTextNode(Integer.toString(((Shape) temp).getPosition().x)));
			else
				xPos.appendChild(doc.createTextNode("null"));
			staff.appendChild(xPos);

			Element yPos = doc.createElement("YPosition");
			if (((Shape) temp).getPosition() != null)
				yPos.appendChild(doc.createTextNode(Integer.toString(((Shape) temp).getPosition().y)));
			else
				yPos.appendChild(doc.createTextNode("null"));
			staff.appendChild(yPos);

			Element inCol = doc.createElement("InColor");
			if (((Shape) temp).getFillColor() != null) {
				inCol.appendChild(doc.createTextNode( String.valueOf(((Shape) temp).getColor().getRGB())));
			} else {
				inCol.appendChild(doc.createTextNode("null"));
			}

			staff.appendChild(inCol);

			Element outCol = doc.createElement("OutColor");

			if (((Shape) temp).getColor() != null) {
				String rgbOut = String.valueOf(((Shape) temp).getColor().getRGB());
				outCol.appendChild(doc.createTextNode(rgbOut));
			} else {
				outCol.appendChild(doc.createTextNode("null"));
			}
			staff.appendChild(outCol);

			Element findMap = doc.createElement("findMap");

			if (((Shape) temp).getProperties() != null) {
				findMap.appendChild(doc.createTextNode("true"));
			} else {
				findMap.appendChild(doc.createTextNode("false"));
			}
			staff.appendChild(findMap);
			Map<String, Double> properities = new HashMap<String, Double>();
			properities = ((Shape) temp).getProperties();
			if (properities != null) {
				
				for (Entry<String, Double> e : properities.entrySet()) {
					Element property = doc.createElement(e.getKey());
					if (e.getValue() != null) {
						property.appendChild(doc.createTextNode(e.getValue().toString()));
					} else {
						property.appendChild(doc.createTextNode("null"));
					}
					staff.appendChild(property);
				}
			}

		}
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("120write", e);
		}
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(savingPath));

		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);

		try {
			transformer.transform(source, result);
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("133write", e);
		}

	}

}
