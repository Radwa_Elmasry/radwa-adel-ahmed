package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class JsonReader {

	public ArrayList<Shape> readJson(String savingPath) {
		// savingPath = "/debug/radwa.json";
		File file = new File(savingPath);
		ArrayList<Shape> shapes = new ArrayList<Shape>();
		Scanner sc = null;
		try {
			sc = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		boolean position = false;
		boolean type = false;
		boolean color = false;
		boolean properties = false;
		String found = null;
		Shape sh = null;
		while (!sc.hasNext("]")) {

			Map<String, Double> propertiesLoaded = new HashMap<>();
			String s = sc.nextLine();
			if (s.contains("{") || s.contains("\"SavedShapes\"")
					|| s.contains("[") || s.contains("},")) {
				continue;
			} else if (s.contains("\"shape\"")) {
				sh = new NewShape();
				String selected = s.replaceAll(" ", "");
				selected = selected.substring(14);
				selected = selected.substring(0, selected.length() - 2);

				try {
					sh = (Shape) Class.forName(selected.trim()).newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
					throw new RuntimeException("52", e);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					throw new RuntimeException("56", e);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					throw new RuntimeException("60", e);
				}

				type = true;
			} else if (s.contains("\"XPosition\"")) {
				String x = s.replaceAll(" ", "");
				x = x.substring(13);
				x = x.substring(0, x.length() - 2);
				s = sc.nextLine();
				String y = s.replaceAll(" ", "");
				y = y.substring(13);
				if (y.contains(",")) {
					y = y.substring(0, y.length() - 2);
				} else {
					y = y.substring(0, y.length() - 1);
				}

				if (!x.equals("-1") && !y.equals("-1")) {
					((Shape) sh).setPosition(new Point(Integer.parseInt(x),
							Integer.parseInt(y)));

				}
				position = true;
			} else if (s.contains("\"InColor\"")) {
				String in = s.replaceAll(" ", "");
				in = in.substring(11);
				if (in.contains(",")) {
					in = in.substring(0, in.length() - 2);
				} else {
					in = in.substring(0, in.length() - 1);
				}
				s = sc.nextLine();
				String out = s.replaceAll(" ", "");
				out = out.substring(12);
				if (out.contains(",")) {
					out = out.substring(0, out.length() - 2);
				} else {
					out = out.substring(0, out.length() - 1);
				}
				if (!in.equals("-1")) {
					int colorI = Integer.parseInt(in);
					((Shape) sh).setFillColor(new Color(colorI));
				} else if (!out.equals("-1")) {
					int colorI = Integer.parseInt(in);
					((Shape) sh).setColor(new Color(colorI));
				}
				color = true;
			} else if (s.contains("\"FindMap\"")) {
				found = s.replaceAll(" ", "");
				found = found.substring(11);
				if (found.contains(",")) {
					found = found.substring(0, found.length() - 2);
				} else {
					found = found.substring(0, found.length() - 1);
				}

				if (found.equals("false")
						|| (found.equals("true") && sc.hasNext("}"))
						|| (found.equals("true") && sc.hasNext("},"))) {
					properties = true;
				}
			}

			else {
				if (found.equals("true")) {
					System.out.println(((Shape) sh).getClass().toString()
							+ " noo");
					Set<String> keys = ((Shape) sh).getProperties().keySet();
					for (String st : keys) {
						if (s.contains(st)) {

							String property = s.replaceAll(" ", "");
							property = property.substring(4 + st.length());
							if (property.contains(",")) {
								property = property.substring(0,
										property.length() - 2);
							} else {
								property = property.substring(0,
										property.length() - 1);
							}
							System.out.println(property);
							if (!property.equals("-1")) {
								propertiesLoaded.put(st,
										Double.valueOf(property));
							} else {
								propertiesLoaded.put(st, null);
							}
						}
						s = sc.nextLine();
					}
					((Shape) sh).setProperties(propertiesLoaded);
					properties = true;
				}

			}

			if (position && color && type && properties) {
				shapes.add(((Shape) sh));
				position = false;
				color = false;
				type = false;
				properties = false;
				found = "-1";
			}
		}
		sc.close();
		return shapes;
	}

}
