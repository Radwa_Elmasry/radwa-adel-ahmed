package eg.edu.alexu.csd.oop.draw;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class JsonWriter {

	public void writeJson(String savingPath, Shape[] shapes) {
		//savingPath = "/debug/radwa.json";
		File file = new File(savingPath);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		FileWriter fw = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		try {
			bw.write("{");
			bw.newLine();
			bw.write("      \"SavedShapes\" :"); // 6 spaces
			bw.newLine();
			bw.write("                         [");
			bw.newLine();
			for (int i = 0; i < shapes.length; i++) {
				Object temp = shapes[i];
				bw.write("                            {");
				bw.newLine();

				bw.write("                            \"shape\": \"" + ((Shape) temp).
						getClass().toString() + "\",");

				bw.newLine();
				if (((Shape) temp).getPosition() != null) {
					bw.write("                            \"XPosition\": \""
							+ Integer.toString(((Shape) temp).getPosition().x) + "\",");
				} else {
					bw.write("                            \"XPosition\": \"" + "-1" + "\",");
				}
				bw.newLine();
				if (((Shape) temp).getPosition() != null) {
					bw.write("                            \"YPosition\": \""
							+ Integer.toString(((Shape) temp).getPosition().y) + "\",");
				} else {
					bw.write("                            \"YPosition\": \"" + "-1" + "\",");
				}
				bw.newLine();
				if (((Shape) temp).getFillColor() != null) {

					bw.write("                            \"InColor\": \""
							+ String.valueOf(((Shape) temp).getFillColor().getRGB()) + "\",");
				} else {
					bw.write("                            \"InColor\": \"" + "-1" + "\",");
				}
				bw.newLine();
				if (((Shape) temp).getColor() != null) {

					bw.write("                            \"OutColor\": \""
							+ String.valueOf(((Shape) temp).getColor().getRGB()) + "\",");
				} else {
					bw.write("                            \"OutColor\": \"" + "-1" + "\",");
				}
				bw.newLine();
				int count = 0;

				if (((Shape) temp).getProperties() != null) {
					bw.write("                            \"FindMap\": \"" + "true" + "\",");
					
				} else {
					bw.write("                            \"FindMap\": \"" + "false" + "\"");
				}
				bw.newLine();
				Map<String, Double> properities = new HashMap<String, Double>();
				properities = ((Shape) temp).getProperties();
				if (properities != null) {

					for (Entry<String, Double> e : properities.entrySet()) {
						if (count == shapes[i].getProperties().size() - 1) {
							bw.write("                            \"" + e.getKey().replaceAll(" ", "") + "\": ");
							if (e.getValue() != null) {
								bw.write("\"" + Double.toString(e.getValue()) + "\"");
							} else {
								bw.write("\"" + "-1" + "\"");
							}
						} else {
							bw.write("                            \"" + e.getKey().replaceAll(" ", "") + "\": ");
							if (e.getValue() != null) {
								bw.write("\"" + Double.toString(e.getValue()) + "\",");
							} else {
								bw.write("\"" + "-1" + "\",");
							}

						}
						bw.newLine();
						count++;
					}

				}
				if (i == shapes.length - 1) {
					bw.write("                            }");
				} else {
					bw.write("                            },");
				}
				bw.newLine();
			}
			bw.write("                         ]");
			bw.newLine();
			bw.write("}");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
