 package eg.edu.alexu.csd.oop.draw;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.LinkedList;
import java.util.List;

public class GuiPaint extends JFrame {

	private static final long serialVersionUID = 1L;
	private JMenuBar menubar = new JMenuBar();
	private JMenu file = new JMenu("File");
	private JMenu edit = new JMenu("Edit");
	private JMenu help = new JMenu("Help");
	private JMenuItem save = new JMenuItem("Save");
	private JMenuItem load = new JMenuItem("Load");
	private JMenuItem undo = new JMenuItem("Undo");
	private JMenuItem redo = new JMenuItem("Redo");
	private JMenuItem move = new JMenuItem("Move");
	private JMenuItem resize = new JMenuItem("Resize");
	private JMenuItem delete = new JMenuItem("Delete");
	private JMenuItem about = new JMenuItem("About");
	private JToolBar toolbarShapes = new JToolBar();
	private JToolBar toolbarEdit = new JToolBar();
	private JButton saveB, loadB, undoB, redoB, moveB, resizeB, deleteB, fillColor, boundaryColor,
		 b, circle, ellipse, lineSeg, triangle, rectangle, square, select;
	private Color trans = new Color(0, 0, 0, 0);
	private Color inColor = trans;
	private Color outColor = (Color.BLACK);
	private Shape selectedShape;
	private int x, y;
	private JPanel mousePanel;
	private JLabel statusBar;
	private LinkedList<Point> points = new LinkedList<Point>();
	private FlowLayout FL = new FlowLayout();
	private JFileChooser chooser = new JFileChooser();

	private FileNameExtensionFilter filterXML = new FileNameExtensionFilter("xml files (*.xml)", "xml");
	private FileNameExtensionFilter filterJSON = new FileNameExtensionFilter("JSON", "json");
	private ClassLoader cl = this.getClass().getClassLoader();
	public GuiPaint() {

		this.setSize(900, 700);
		this.setTitle("Paint");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
		this.setTitle("Paint");
		this.setJMenuBar(menubar);
		menubar.add(file);
		file.addSeparator();
		file.add(save);
		file.add(load);
		file.addSeparator();
		menubar.add(edit);
		edit.add(undo);
		edit.add(redo);
		edit.addSeparator();
		edit.addSeparator();
		edit.add(move);
		edit.add(resize);
		edit.add(delete);
		menubar.add(help);
		help.add(about);
	
		this.add(toolbarShapes, BorderLayout.SOUTH);
		toolbarShapes.setBackground(Color.lightGray);
		for (int i = 0; i < 12; i++) {
			toolbarShapes.addSeparator();
		}

		undoB = new JButton();
		redoB = new JButton();
		moveB = new JButton();
		resizeB = new JButton();
		deleteB = new JButton();
		saveB = new JButton();
		loadB = new JButton();
		fillColor = new JButton();
		boundaryColor = new JButton();
		
		undoB.setToolTipText("Undo");
		redoB.setToolTipText("Redo");
		moveB.setToolTipText("Move");
		resizeB.setToolTipText("Resize");
		deleteB.setToolTipText("Delete");
		saveB.setToolTipText("Save");
		loadB.setToolTipText("Load");
		fillColor.setToolTipText("Fill Color");
		boundaryColor.setToolTipText("Boundary Color");
  
		undoB.setIcon(new ImageIcon(cl.getResource("images/undo.png")));
		redoB.setIcon(new ImageIcon(cl.getResource("images/redo.png")));
		moveB.setIcon(new ImageIcon(cl.getResource("images/move.png")));
		resizeB.setIcon(new ImageIcon(cl.getResource("images/resize.png")));
		deleteB.setIcon(new ImageIcon(cl.getResource("images/delete.png")));
		saveB.setIcon(new ImageIcon(cl.getResource("images/save.png")));
		loadB.setIcon(new ImageIcon(cl.getResource("images/load.png")));
		fillColor.setIcon(new ImageIcon(cl.getResource("images/fill.png")));
		boundaryColor.setIcon(new ImageIcon(cl.getResource("images/border.png")));
		statusBar = new JLabel("Mouse Motion");
		statusBar.setForeground(Color.BLUE);
		
		select = new JButton();
		select.setIcon(new ImageIcon(cl.getResource("images/select.png")));

		this.add(toolbarEdit, BorderLayout.NORTH);
		toolbarEdit.setBackground(Color.lightGray);
		toolbarEdit.setLayout(FL);
		FL.setAlignment(FlowLayout.LEFT);
		toolbarEdit.add(saveB);
		toolbarEdit.add(loadB);
		toolbarEdit.add(moveB);
		toolbarEdit.add(resizeB);
		toolbarEdit.add(undoB);
		toolbarEdit.add(redoB);
		toolbarEdit.add(deleteB);
		toolbarEdit.add(fillColor);
		toolbarEdit.add(boundaryColor);
		toolbarEdit.add(select);
		toolbarEdit.addSeparator();
		for (int i = 0; i < 12; i++) {
			toolbarEdit.addSeparator();
		}
		toolbarEdit.add(statusBar);
		Colors c = new Colors();
		fillColor.addActionListener(c);
		boundaryColor.addActionListener(c);

		mousePanel = new MyPanel();
		mousePanel.setBackground(Color.white);
		this.add(mousePanel, BorderLayout.CENTER);

		HandlerClass handler = new HandlerClass();
		mousePanel.addMouseListener(handler);
		mousePanel.addMouseMotionListener(handler);

		validate();
	}

	public JPanel getMousePanel() {
		return mousePanel;
	}

	private class Colors implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) {
			JButton src = (JButton) event.getSource();

			if (src.equals(fillColor)) {
				inColor = JColorChooser.showDialog(null, "Pick your fill color", inColor);
				if (inColor == null) {
					inColor = Color.WHITE;
				}

			}

			if (src.equals(boundaryColor)) {
				outColor = JColorChooser.showDialog(null, "Pick your boundary color", outColor);
				if (outColor == null) {
					inColor = Color.BLACK;
				}
			}

		}

	}

	private class HandlerClass implements MouseListener, MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent event) { // useInMove
			statusBar.setText(String.format("Dragged at : %d , %d", event.getX(), event.getY()));
		}

		@Override
		public void mouseMoved(MouseEvent event) {
			statusBar.setText(String.format("Dim : %d , %d", event.getX(), event.getY()));

		}

		@Override
		public void mouseClicked(MouseEvent event) {
			x = event.getX();
			y = event.getY();
			points.add(new Point(x, y));
			statusBar.setText(String.format("Clicked at : %d , %d", x, y));
		}

		@Override
		public void mouseEntered(MouseEvent event) {
			statusBar.setText("Mouse Entered");
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			statusBar.setText("Mouse Exited");
		}

		@Override
		public void mousePressed(MouseEvent event) {
			statusBar.setText(String.format("Pressed at : %d , %d", event.getX(), event.getY()));
		}

		@Override
		public void mouseReleased(MouseEvent event) {
			statusBar.setText(String.format("Released at : %d , %d", event.getX(), event.getY()));
		}

	}

	void addDrawingListener(ActionListener listen) {
		if (b != null) {
			b.addActionListener(listen);
		}
		circle.addActionListener(listen);
		triangle.addActionListener(listen);
		lineSeg.addActionListener(listen);
		square.addActionListener(listen);
		rectangle.addActionListener(listen);
		ellipse.addActionListener(listen);
		points.clear(); 

	}

	void addHistoryListener(ActionListener listen) {

		undo.addActionListener(listen);
		undoB.addActionListener(listen);
		redo.addActionListener(listen);
		redoB.addActionListener(listen);

	}

	void addUpdateListener(ActionListener listen) {
		select.addActionListener(listen);
		resize.addActionListener(listen);
		resizeB.addActionListener(listen);
		move.addActionListener(listen);
		moveB.addActionListener(listen);
		delete.addActionListener(listen);
		deleteB.addActionListener(listen);

	}

	void addSaveLoadListener(ActionListener listen) {
		save.addActionListener(listen);
		saveB.addActionListener(listen);
		load.addActionListener(listen);
		loadB.addActionListener(listen);

	}
	
	void createButtons (List<Class<? extends Shape>> list){
		this.setVisible(true);
		
		for (int i = 0; i < list.size(); i++) {
			String[] exten = list.get(i).getName().split("\\.");
			String s = exten[exten.length - 1];
			if (s.equals("Circle")) {
				circle = new JButton();
				circle.setName(s);
				circle.setToolTipText(s);
				try{
				circle.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				}
				catch (NullPointerException e){
					circle.setText(s);
				}
				toolbarShapes.add(circle);
				toolbarShapes.addSeparator();
			   }

			else if (s.equals("Ellipse")) {
				ellipse = new JButton();
				ellipse.setName(s);
				ellipse.setToolTipText(s);
				try {
					ellipse.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				} catch (NullPointerException e) {
					ellipse.setText(s);
				}
			
				toolbarShapes.add(ellipse);
				toolbarShapes.addSeparator();
			}

			else if (s.equals("Square")) {
				square = new JButton();
				square.setName(s);
				square.setToolTipText(s);
				try {
					square.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				} catch (NullPointerException e) {
					square.setText(s);
				}
				
				toolbarShapes.add(square);
				toolbarShapes.addSeparator();
		       	}

			else if (s.equals("Rectangle")) {
				rectangle = new JButton();
				rectangle.setName(s);
				rectangle.setToolTipText(s);
				try {
					rectangle.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				} catch (NullPointerException e) {
					rectangle.setText(s);
				}
				
				toolbarShapes.add(rectangle);
				toolbarShapes.addSeparator();
			    }

			else if (s.equals("LineSegment")) {
				lineSeg = new JButton();
				lineSeg.setName(s);
				lineSeg.setToolTipText(s);
				try {
					lineSeg.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				} catch (NullPointerException e) {
					lineSeg.setText(s);
				}
			
				toolbarShapes.add(lineSeg);
				toolbarShapes.addSeparator();
			    }

			else if (s.equals("Triangle")) {
				triangle = new JButton();
				triangle.setName(s);
				triangle.setToolTipText(s);
				try {
					triangle.setIcon(new ImageIcon(cl.getResource("images/" + s + ".png")));
				} catch (NullPointerException e) {
					triangle.setText(s);
				}
				
				toolbarShapes.add(triangle);
				toolbarShapes.addSeparator();
		     	}

			else {
				b = new JButton(s);
				b.setName(s);
				b.setToolTipText(s);
				toolbarShapes.add(b);
				toolbarShapes.addSeparator();
			}

		}
		
	}

	public LinkedList<Point> getPoints() {
		return points;

	}

	public Point getPos() {
		return new Point(x, y);

	}

	public Color getOutColor() {
		return outColor;
	}

	public Color getInColor() {
		return inColor;
	}

	public JMenuItem getUndo() {
		return undo;
	}

	public JMenuItem getRedo() {
		return redo;
	}

	public JButton getUndoB() {
		return undoB;
	}

	public JButton getRedoB() {
		return redoB;
	}

	public JMenuItem getMove() {
		return move;
	}

	public JMenuItem getResize() {
		return resize;
	}

	public JButton getMoveB() {
		return moveB;
	}

	public JMenuItem getDelete() {
		return delete;
	}

	public JMenuItem getSave() {
		return save;
	}

	public JMenuItem getLoad() {
		return load;
	}

	public JButton getSaveB() {
		return saveB;
	}

	public JButton getLoadB() {
		return loadB;
	}

	public JButton getDeleteB() {
		return deleteB;
	}

	public JButton getResizeB() {
		return resizeB;
	}

	public Shape getSelectedShape() {
		return selectedShape;
	}

	public JFileChooser getChooser() {
		return chooser;
	}

	public FileNameExtensionFilter getFilterXML() {
		return filterXML;
	}

	public FileNameExtensionFilter getFilterJSON() {
		return filterJSON;
	}

	public JButton getSelect() {
		return select;
	}
	


}
