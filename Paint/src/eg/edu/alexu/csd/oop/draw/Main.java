package eg.edu.alexu.csd.oop.draw;

public class Main {

	public static void main(String[] args) {
	
		GuiPaint theView = new GuiPaint();

		Engine theModel = Engine.getInstance();

		new EngineController(theModel, theView);

	}

}
