 package eg.edu.alexu.csd.oop.draw;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MyClassLoader {
	

	@SuppressWarnings("unchecked")
	public List <Class < ? extends Shape>> loadClasses() {
		 
		  List<Class < ? extends Shape>> classes = new ArrayList<Class < ? extends Shape>>();
		  LinkedList<JarFile> jarList = new LinkedList<JarFile>();
		  String string = System.getProperty("java.class.path");
		  String[] strar = string.split(File.pathSeparator);
		  
		  for (int i = 1; i < strar.length; i++) {	  
			  if(strar[i].contains(".jar")){
			try {
				jarList.add(new JarFile(strar[i]));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			  }
			  
		}
	 
		for(int j = 0 ; j < jarList.size() ; j++){
		Enumeration<?> e = jarList.get(j).entries();
		while (e.hasMoreElements()) {
	        JarEntry je = (JarEntry) e.nextElement();
	        if(je.isDirectory() || !je.getName().endsWith(".class")){
	            continue;
	        }
	    // -6 because of .class
	    String className = je.getName().substring(0,je.getName().length()-6);
	    className = className.replace('/', '.');
	    try {
			Class<?> cls = Class.forName(className);
			if(Shape.class.isAssignableFrom(cls))
			{
				classes.add((Class<? extends Shape>) cls);
				//System.out.println(cls.getName());
			}
		} catch (ClassNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		} 
		 try {
			 jarList.get(j).close();
	        } catch (IOException e2) {
	            // TODO Auto-generated catch block
	            e2.printStackTrace();
	        }
		
		}
		
		return classes;
		
			}

	public Shape shapesFactory ( String s) {
		
		Shape reflectionShape = null;
		
		for (int i = 0; i < loadClasses().size(); i++) {
			
			try {
				if (loadClasses().get(i) == Class.forName(s)) {
					 reflectionShape = (Shape) loadClasses().get(i).newInstance();
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 throw new RuntimeException("reflection",  e);
			
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 throw new RuntimeException("reflection",  e);
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 throw new RuntimeException("reflection",  e);
			}
			
		}
		return reflectionShape;
		
	}

}

