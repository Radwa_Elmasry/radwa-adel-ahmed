package eg.edu.alexu.csd.oop.draw;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class NewShape implements Shape, Cloneable {

	private Point position = null;
	private Color inColor = null; 
	private Color outColor = null;
	private Map<String, Double> properties = new HashMap<>();

	@Override
	public void setPosition(Point position) {
		this.position = position;
	}

	@Override
	public Point getPosition() {

		return this.position;
	}

	@Override
	public Map<String, Double> getProperties() {
	
		return this.properties;
	}

	@Override
	public void setProperties(Map<String, Double> properties) {
		this.properties = properties;
	}

	@Override
	public void setColor(Color color) {
		this.outColor = color;
	}

	@Override
	public Color getColor() {
		return this.outColor;
	}

	@Override
	public void setFillColor(Color color) {
		this.inColor = color;
	}

	@Override
	public Color getFillColor() {
		return this.inColor;
	}

	@Override
	public void draw(Graphics canvas) {
	
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		Map<String, Double> propertiesNew ;
		MyClassLoader loadJar = new MyClassLoader();
		Shape newShape = loadJar.shapesFactory(this.getClass().getName());
		Point newPoint = new Point(this.getPosition());
		System.out.println(this.getPosition());
		propertiesNew = new HashMap<>();
		propertiesNew = this.getProperties();
		Color newColor = this.getColor();
		newShape.setColor(newColor);
		Color newFillColor = this.getFillColor();
		newShape.setFillColor(newFillColor);
		System.out.println(propertiesNew);
		newShape.setPosition(newPoint);
		newShape.setProperties(propertiesNew);
		
		return newShape;
	}

}
	