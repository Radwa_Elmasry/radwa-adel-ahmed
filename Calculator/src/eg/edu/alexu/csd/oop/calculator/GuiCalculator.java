package eg.edu.alexu.csd.oop.calculator;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class GuiCalculator extends JFrame {

	private JTextField answerField;
	private JButton one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mult, div, equal, prev,
			current, next, save, load, clear, decimal, negative, back;
	String Snum1, Snum2, Sans, input;
	String print = null;
	double answer = 0.0;
	private JPanel contentpanel;
	private boolean equalsClicked = false, opChosen = false;
	char operation = ' ';

	public GuiCalculator() {
		super("Calculator"); // title
		Font font1 = new Font("SansSerif", Font.BOLD, 25);
		answerField = new JTextField(null, 20);
		answerField.setFont(font1);
		answerField.setBackground(Color.LIGHT_GRAY);
		answerField.setForeground(Color.darkGray);
		answerField.setEditable(false);

		one = new JButton("1");
		two = new JButton("2");
		three = new JButton("3");
		four = new JButton("4");
		five = new JButton("5");
		six = new JButton("6");
		seven = new JButton("7");
		eight = new JButton("8");
		nine = new JButton("9");
		zero = new JButton("0");
		decimal = new JButton(".");
		negative = new JButton("+/-");
		back = new JButton("DEL");
		add = new JButton("+");
		sub = new JButton("-");
		mult = new JButton("*");
		div = new JButton("/");
		equal = new JButton("=");
		prev = new JButton("previous");
		current = new JButton("CRT");
		next = new JButton("next");
		save = new JButton("save");
		load = new JButton("load");
		clear = new JButton("AC");

		mult.setFont(font1);
		div.setFont(font1);
		sub.setFont(font1);
		add.setFont(font1);
		equal.setFont(font1);
		negative.setFont(font1);
		decimal.setFont(font1);

		Dimension dim = new Dimension(75, 50);

		one.setPreferredSize(dim);
		two.setPreferredSize(dim);
		three.setPreferredSize(dim);
		four.setPreferredSize(dim);
		five.setPreferredSize(dim);
		six.setPreferredSize(dim);
		seven.setPreferredSize(dim);
		eight.setPreferredSize(dim);
		nine.setPreferredSize(dim);
		zero.setPreferredSize(dim);
		decimal.setPreferredSize(dim);
		negative.setPreferredSize(dim);
		back.setPreferredSize(dim);
		add.setPreferredSize(dim);
		sub.setPreferredSize(dim);
		mult.setPreferredSize(dim);
		div.setPreferredSize(dim);
		equal.setPreferredSize(dim);
		answerField.setPreferredSize(new Dimension(400, 80));
		clear.setPreferredSize(dim);
		prev.setPreferredSize(new Dimension(100, 50));
		current.setPreferredSize(dim);
		next.setPreferredSize(new Dimension(100, 50));
		save.setPreferredSize(new Dimension(100, 50));
		load.setPreferredSize(new Dimension(100, 50));

		Numbers n = new Numbers();
		Calc c = new Calc();
		PrevNext p = new PrevNext();
		Features f = new Features();

		one.addActionListener(n);
		two.addActionListener(n);
		three.addActionListener(n);
		four.addActionListener(n);
		five.addActionListener(n);
		six.addActionListener(n);
		seven.addActionListener(n);
		eight.addActionListener(n);
		nine.addActionListener(n);
		zero.addActionListener(n);
		
		decimal.addActionListener(f);
		negative.addActionListener(f);
		back.addActionListener(f);

		add.addActionListener(c);
		sub.addActionListener(c);
		mult.addActionListener(c);
		div.addActionListener(c);
		equal.addActionListener(c);
		clear.addActionListener(c);

		prev.addActionListener(p);
		next.addActionListener(p);
		current.addActionListener(p);
		save.addActionListener(p);
		load.addActionListener(p);

		contentpanel = new JPanel();
		contentpanel.setBackground(Color.ORANGE);
		contentpanel.setLayout(new FlowLayout());
		contentpanel.add(answerField, BorderLayout.NORTH);
		contentpanel.add(prev);
		contentpanel.add(next);
		contentpanel.add(save);
		contentpanel.add(load);
		contentpanel.add(seven);
		contentpanel.add(eight);
		contentpanel.add(nine);
		contentpanel.add(back);
		contentpanel.add(clear);
		contentpanel.add(four);
		contentpanel.add(five);
		contentpanel.add(six);
		contentpanel.add(mult);
		contentpanel.add(div);
		contentpanel.add(one);
		contentpanel.add(two);
		contentpanel.add(three);
		contentpanel.add(add);
		contentpanel.add(sub);
		contentpanel.add(zero);
		contentpanel.add(decimal);
		contentpanel.add(negative);
		contentpanel.add(current);
		contentpanel.add(equal);

		this.setContentPane(contentpanel);
	}

	private class Numbers implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) throws RuntimeException {
			// TODO Auto-generated method stub

			String x = ((JButton) event.getSource()).getText();
			JButton src = (JButton) event.getSource();

			if (opChosen == false) {
				if (Snum1 == null) {
					Snum1 = x;
				} else {
					Snum1 += x;
				}
			} else {
				if (Snum2 == null) {
					Snum2 = x;
				} else {
					Snum2 += x;
				}
			}
			if (equalsClicked == false) {
				if (opChosen == false) {
					answerField.setText(Snum1);
				} else {
					answerField.setText(Snum2);
				}
			}

			if (opChosen == false) {
				answerField.setText(Snum1);
			} else {
				answerField.setText(Snum2);
			}

		}
	}

		private class Features implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent event) throws RuntimeException {
				// TODO Auto-generated method stub

				JButton src = (JButton) event.getSource();
		if (src.equals(decimal)) {
			if (opChosen == false) {
				if (Snum1 == null) {
					Snum1 = "0.";
				} else if (Snum1 != null) {
					if (Snum1.contains(".")) {
						answerField.setText("You already have a decimal point!");
						throw new RuntimeException("You already have a decimal point!");
					} else {
						Snum1 += ".";
					}

				}
			} else {
				if (Snum2 == null) {
					Snum2 = "0.";
				} else if (Snum2 != null) {
					if (Snum2.contains(".")) {
						answerField.setText("You already have a decimal point!");
						throw new RuntimeException("You already have a decimal point!");
					} else {
						Snum2 += ".";
					}

				}
			}
		}

		if (src.equals(negative)) {
			if (opChosen == false) {
				if (Snum1 == null) {
					Snum1 = "-";
				} else if (Snum1 != null && Snum1.startsWith("-")) {
					Snum1 = Snum1.substring(1); // return positive
				} else {
					Snum1 = "-" + Snum1;
				}
			} else {
				if (opChosen == false) {
					if (Snum2 == null) {
						Snum2 = "-";
					} else if (Snum2 != null && Snum2.startsWith("-")) {
						Snum2 = Snum2.substring(1); // return positive
					} else {
						Snum2 = "-" + Snum2;

					}
				}
			}
		}

		if (src.equals(back)) {
			if (opChosen == false) {
				if (Snum1 == null) {
					throw new RuntimeException("CAN'T DELETE NOTHING!");
				} else {
					Snum1 = Snum1.substring(0, Snum1.length() - 1);
				}
			} else {
				if (Snum2 == null) {
					throw new RuntimeException("CAN'T DELETE NOTHING!");
				} else {
					Snum2 = Snum2.substring(0, Snum2.length() - 1);
				}
			}
		}
		if (equalsClicked == false) {
			if (opChosen == false) {
				answerField.setText(Snum1);
			} else {
				answerField.setText(Snum2);
			}
		}

		if (opChosen == false) {
			answerField.setText(Snum1);
		} else {
			answerField.setText(Snum2);
		}

		
	}
		}

	private class Calc implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) throws RuntimeException {
			// TODO Auto-generated method stub

			JButton src = (JButton) event.getSource();

			if (src.equals(add)) {
				if (Snum1 == null) {
					answerField.setText("Choose your numbers first!");
					throw new RuntimeException("Choose your numbers first!");
				} else if (Snum1 != null && Snum2 == null) {
					opChosen = true;
					operation = '+';
				} else if (Snum1 != null && Snum2 != null) {
					answerField.setText("Two Operations Only!");
					throw new RuntimeException("Two Operations Only!");
				}
			}

			if (src.equals(sub)) {
				if (Snum1 == null) {
					answerField.setText("Choose your numbers first!");
					throw new RuntimeException("Choose your numbers first!");
				} else if (Snum1 != null && Snum2 == null) {
					opChosen = true;
					operation = '-';
				} else if (Snum1 != null && Snum2 != null) {
					answerField.setText("Two Operations Only!");
					throw new RuntimeException("Two Operations Only!");
				}
			}

			if (src.equals(mult)) {
				if (Snum1 == null) {
					answerField.setText("Choose your numbers first!");
					throw new RuntimeException("Choose your numbers first!");
				} else if (Snum1 != null && Snum2 == null) {
					opChosen = true;
					operation = '*';
				} else if (Snum1 != null && Snum2 != null) {
					answerField.setText("Two Operations Only!");
					throw new RuntimeException("Two Operations Only!");
				}
			}

			if (src.equals(div)) {
				if (Snum1 == null) {
					answerField.setText("Choose your numbers first!");
					throw new RuntimeException("Choose your numbers first!");
				} else if (Snum1 != null && Snum2 == null) {
					opChosen = true;
					operation = '/';
				} else if (Snum1 != null && Snum2 != null) {
					answerField.setText("Two Operations Only!");
					throw new RuntimeException("Two Operations Only!");
				}
			}

			if (src.equals(equal)) {
				equalsClicked = true;
				CalculatorEngine g = CalculatorEngine.getInstance();
				if (g.lastIsprev || g.lastIsNext) {
					g.lastIsprev = false;
					g.lastIsNext = false;
					String strarr[] = print.split("((?<=\\+)|(?=\\+)|(?<=-)|(?=-)|(?<=/)|(?=/)|(?<=\\*)|(?=\\*))");

					Snum1 = strarr[0];
					String oper = strarr[1];
					Snum2 = strarr[2];

					operation = oper.charAt(0);

				}

				if (Snum1 == null) {
					throw new RuntimeException("Choose your numbers first!");
				} else if (Snum1 != null && Snum2 == null) {
					throw new RuntimeException("Choose both numbers first!");
				}

				if (operation == '/' && Double.parseDouble(Snum2) == 0.0) {
					answerField.setText("Math ERROR!");
					throw new RuntimeException("Can't Divide by ZERO!");
				} else if (Snum1 != null && Snum2 != null) {
					input = Snum1 + Character.toString(operation) + Snum2;
					CalculatorEngine result = CalculatorEngine.getInstance();
					result.lastIsprev = false;
					result.lastIsNext = false;
					result.input(input);
					answer = Double.parseDouble(result.getResult());
				}

				Sans = Double.toString(answer);
				answerField.setText(Sans);

			}

			if (src.equals(clear)) {
				Snum1 = null;
				Snum2 = null;
				equalsClicked = false;
				opChosen = false;
				operation = ' ';
				answerField.setText(null);
				Sans = null;
				CalculatorEngine g = CalculatorEngine.getInstance();
				g.numClear++;		
		    	g.counterNext = 0;
				g.temp = 0;
				g.countLoadPrev = 0;
				g.countLoadNext = 0;
				g.tempLoad = 0;
				g.lastIsprev = false;
				g.lastIsNext = false;
			}

		}

	}

	private class PrevNext implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent event) throws RuntimeException {
			// TODO Auto-generated method stub

			JButton src = (JButton) event.getSource();
			CalculatorEngine engine = CalculatorEngine.getInstance();

			if (src.equals(prev)) {
				print = engine.prev();
				if (print == null) {
					answerField.setText("NO PREVIOUS!");
				} else {
					answerField.setText(print);
				}

			}

			if (src.equals(next)) {
				print = engine.next();
				if (print == null) {
					answerField.setText("NO NEXT!");
				} else {
					answerField.setText(print);
				}

			}

			if (src.equals(current)) {
				print = engine.current();
				if (print == null) {
					answerField.setText("NO CURRENT!");
				} else {
					answerField.setText(print);
				}

			}

			if (src.equals(save)) {
				engine.save();
				answerField.setText("SAVED");
			}

			if (src.equals(load)) {

				engine.load();
				answerField.setText("LOAD");

			}

		}
	}
}
