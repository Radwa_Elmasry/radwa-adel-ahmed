package eg.edu.alexu.csd.oop.calculator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

public class SavePermenantFile {

	public void savePermenant() throws IOException {
		Writer writer = null;
		CalculatorEngine engine = CalculatorEngine.getInstance();
		String current = engine.current();
		int indexCurrent = engine.history.indexOf(current);
		String line;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("saved.txt"), "utf-8"));
			for (int i = 0; i <= indexCurrent; i++) {
				line = engine.history.get(i);
				writer.write(line);
				writer.write(System.lineSeparator());
			}
		} catch (IOException ex) {
			// report
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}
	}
}
