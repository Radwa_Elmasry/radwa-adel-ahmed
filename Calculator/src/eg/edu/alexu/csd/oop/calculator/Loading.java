package eg.edu.alexu.csd.oop.calculator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class Loading {
	private static Loading firstLoad = null;

	private Loading() {

	}

	public static Loading getInstance() {
		if (firstLoad == null) {
			firstLoad = new Loading();
		}
		return firstLoad;
	}

	public static void destoryInstance() {
		firstLoad = null;
	}

	File file = new File("saved.txt");

	Stack<String> temp = new Stack();
	Stack<String> stk = new Stack();

	public void load() throws FileNotFoundException, RuntimeException {
		while (!temp.isEmpty()) {
			temp.pop();
		}
		while (!stk.isEmpty()) {
			stk.pop();
		}

		try {

			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				stringBuffer.append(line);
				stringBuffer.append("\n");

				if (temp.size() <= 5) {
					temp.push(line);
				}
			}
			fileReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		while (!temp.isEmpty()) {
			stk.push(temp.pop());
		}
	}

	public String current() {

		String current = null;
		if (stk.isEmpty()) {
			return null;
		}
		CalculatorEngine g = CalculatorEngine.getInstance();
		if (g.lastIsprev && !g.lastIsNext) {
			current = stk.get(g.countLoadPrev);
		} else if (!g.lastIsprev && g.lastIsNext) {
			current = stk.get(g.countLoadNext);
		} else if (g.currentPrev) {
			current = stk.get(stk.size() - 1);
		} else {
			current = stk.get(0);
		}

		return current;

	}

	public String prev() throws RuntimeException {

		String prev = null;
		CalculatorEngine g = CalculatorEngine.getInstance();

		int i = g.countLoadPrev;
		if (stk.isEmpty() || i >= stk.size()) {
			prev = null;
			g.lastIsprev = false;

		} else {
			if (!stk.isEmpty()) {
				prev = (String) stk.get(i);

			} else {
				prev = null;
			}
		}

		if (prev == null && i == stk.size()) {
		    g.tempLoad = 1;
			g.currentPrev = true;
		}
		return prev;
	}

	public String next() throws RuntimeException {

		String next = null;

		CalculatorEngine g = CalculatorEngine.getInstance();

		int i = g.countLoadPrev;
		int j = g.countLoadNext;
		if (i == 0) {
			next = null;
			g.lastIsNext = false;
		} else {
			if (!stk.isEmpty() && j >= 0) {
				next = (String) stk.get(j);

			} else {
				next = null;
				g.lastIsNext = false;
				g.countLoadPrev = 0;
				g.counterNext = 0;
				g.tempLoad = 0;
			}
		}
		return next;
	}

}
