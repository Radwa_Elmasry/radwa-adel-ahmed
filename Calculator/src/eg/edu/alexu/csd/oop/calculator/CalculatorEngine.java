package eg.edu.alexu.csd.oop.calculator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

public class CalculatorEngine implements Calculator {
	private static CalculatorEngine firstInstance = null;

	private CalculatorEngine() {
		
		Loading.getInstance().destoryInstance();

	}

	public static CalculatorEngine getInstance() {
		if (firstInstance == null) {
			firstInstance = new CalculatorEngine();
		}
		return firstInstance;
	}

	double num1 = 0.0, num2 = 0.0;
	char oper;
	int counterPrev = 0, counterNext = 0, temp = 0, countLoadPrev = 0, countLoadNext = 0, tempLoad = 0;
	 boolean lastIsprev = false;
	 boolean lastIsNext = false;
	 boolean lastIsNull = true;
	 boolean loaded = false;
	 boolean currentPrev = false;
	 int numClear = 0;
    LinkedList<String> history = new LinkedList();

	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		String strarr[] = s.split("((?<=\\+)|(?=\\+)|(?<=-)|(?=-)|(?<=/)|(?=/)|(?<=\\*)|(?=\\*))");

		String Str1 = strarr[0];
		String operation = strarr[1];
		String Str2 = strarr[2];

		num1 = Double.parseDouble(Str1);
		num2 = Double.parseDouble(Str2);
		oper = operation.charAt(0);
		boolean i = lastIsprev;
		boolean j = lastIsNext;
		/*if (!i && !j) {
			counterPrev = 0;
			counterNext = 0;
			temp = 0;
			countLoadPrev = 0;
			countLoadNext = 0;
			tempLoad = 0;
			loaded = false;	
		}*/
		
		if(!history.contains(s) && lastIsNull && !loaded){
			history.addLast(s);
			lastIsprev = false;
			lastIsNext = false;
			currentPrev = false;
			counterPrev = 0;
			counterNext = 0;
			temp = 0;
			countLoadPrev = 0;
			countLoadNext = 0;
			tempLoad = 0;
			loaded = false;	
			
		}else if(!history.contains(s) && counterPrev!= 0 && !loaded && !lastIsNull){
			history.add(counterPrev+1, s);
			int count2 = counterPrev +2;
			for(int f = count2 ; f < history.size() ; f++){
				history.remove(count2);
			}
			counterPrev = 0;
			counterNext = 0;
			temp = 0;
			countLoadPrev = 0;
			countLoadNext = 0;
			tempLoad = 0;
			lastIsprev = false;
			lastIsNext = false;
			lastIsNull = true;
		}else if(!history.contains(s) && countLoadPrev != 0){
			while(!history.isEmpty()){
				history.remove();
			}
			history.addLast(s);
			countLoadPrev = 0;
			countLoadNext = 0;
			tempLoad = 0;
			counterPrev = 0;
			counterNext = 0;
			temp = 0;
			loaded = false;	
			lastIsNull = true;
		}

		if (history.size() > 5) {
			int count = history.size() - 5;
			for (int z = 0; z < count; z++) {
				history.remove(history.get(z));
			}
		}
	}

	@Override
	public String getResult() throws RuntimeException {
		// TODO Auto-generated method stub

		double r = 0.0;
		String result;

		switch (oper) {
		case '+':
			r = num1 + num2;
			break;
		case '-':
			r = num1 - num2;
			break;
		case '*':
			r = num1 * num2;
			break;
		case '/':
			if (num2 == 0) {
				throw new RuntimeException("Can't Divide by Zero");
			}
			r = num1 / num2;
			break;

		}

		result = String.valueOf(r);

		return result;
	}

	@Override
	public String current() {
		// TODO Auto-generated method stub
		String current = null;
		Loading ld = Loading.getInstance();
		if(history.isEmpty()){
			return null;
		}
		if (!loaded || (loaded && numClear >= 1)) {
			if (lastIsprev && !lastIsNext) {
				current = history.get(history.size()-1 - counterPrev);
			} else if (!lastIsprev && lastIsNext) {
				current = history.get(history.size()-1 -counterNext);
			}else if(currentPrev){
				current = history.get(0);
			}
			else {
				current = history.get(history.size()-1);
			}
		} else if (loaded && numClear == 0) {
			current = ld.current();
		}
		return current;
	}

	@Override
	public String prev() {
		// TODO Auto-generated method stub
		String prev = null;
		lastIsprev = true;
		currentPrev = false;
		lastIsNext = false;
		Loading ld = Loading.getInstance();
		if (!loaded || (loaded && numClear >= 1)) {
			counterPrev++;
			int i = counterPrev;
			if (history.isEmpty() || i >= history.size()) {
				prev = null;
			} else {
				if (!history.isEmpty()) {
					prev = history.get(history.size()-1 -i);
				} else {
					prev = null;
				}
			}
		} else if (loaded && numClear == 0) {
			countLoadPrev++;
			prev = ld.prev();
		}
		if (prev != null) {
			input(prev);
			lastIsNull = false;
		}
		if (prev == null && counterPrev ==1) {
			counterPrev--;
			lastIsprev = false;
		}
		if (prev == null && counterPrev >= history.size()) {
			temp = 1;
			counterPrev = history.size();
			lastIsprev = false;
			currentPrev = true;
			lastIsNull = true;
		}else if(prev == null){
			counterPrev--;
		}
	
		return prev;
	}

	@Override
	public String next() {
		// TODO Auto-generated method stub
		String next = null;
		currentPrev = false;
		if(lastIsNext){
			counterPrev+=2;
		}
		lastIsNext = true;
		Loading ld = Loading.getInstance();
		if (!loaded || (loaded && numClear >= 1)) {
			temp++;
			counterNext = counterPrev - temp;
			int i = counterPrev;
			int j = counterNext;

			if (i == 0) {
				next = null;
				lastIsNext = false;
				temp--;
			} else {
				if (!history.isEmpty() && j >= 0) {
					next = history.get(history.size()-1 -j);
					counterPrev-=2;//change
				} else {
					next = null;
				}
				
				if (next == null && j == -1) {
					lastIsNext = false;
					counterPrev = 0;
					counterNext = 0;
					temp = 0;
				}
			}

		} else if (loaded && numClear == 0) {
			tempLoad++;
			countLoadNext = countLoadPrev - tempLoad;
			next = ld.next();
			if (next == null){
				tempLoad--;
				lastIsNext = false;
			}
		}
		if (next != null) {
			input(next);
		}

		return next;
	}

	@Override
	public void save() {
		// TODO Auto-generated method stub
		SavePermenantFile s = new SavePermenantFile();
		Path path = FileSystems.getDefault().getPath("C:/Users/Radwa/git/radwa-adel-ahmed/Calculator", "saved.txt");
		try {
			Files.deleteIfExists(path);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			s.savePermenant();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();
		}

		System.out.println(history.size());
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		loaded = true;
		lastIsNext = false;
		lastIsprev = false;
		Loading l = Loading.getInstance();
		try {
			l.load();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException();

		}
		
		System.out.println(history.size());
		System.out.println(history.indexOf(current()));
	
	}

}
