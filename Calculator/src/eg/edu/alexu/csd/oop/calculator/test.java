package eg.edu.alexu.csd.oop.calculator;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

	 public class test {
		 @Test
		 public void test() {
		  CalculatorEngine n = CalculatorEngine.getInstance();
		 /* n.input("1+2");
		  Assert.assertEquals("3.0", n.getResult());
		  n.input("5-1");
		  Assert.assertEquals("4.0", n.getResult());
		  n.input("9*8");
		  Assert.assertEquals("72.0", n.getResult());
		  n.input("8/4");
		  Assert.assertEquals("8/4", n.current());
		  Assert.assertEquals("9*8", n.prev());
		  Assert.assertEquals("5-1", n.prev());
		  Assert.assertEquals("1+2", n.prev());
		  Assert.assertEquals(null, n.prev());
		  Assert.assertEquals("5-1", n.next());
		  Assert.assertEquals("5-1", n.current());
		  Assert.assertEquals("9*8", n.next());
		  Assert.assertEquals("8/4", n.next());
		  Assert.assertEquals("8/4", n.current());
		  Assert.assertEquals(null, n.next());
		  n.save();
		  n.input("9*4");
		  n.input("8-3");
		  n.input("5-6");
		  n.input("3*2");
		  Assert.assertEquals("3*2", n.current());
		  Assert.assertEquals("5-6", n.prev());
		  Assert.assertEquals("5-6", n.current());
		  Assert.assertEquals("8-3", n.prev());
		  Assert.assertEquals("9*4", n.prev());
		  Assert.assertEquals("8/4", n.prev());
		  Assert.assertEquals("8/4", n.current());
		 // Assert.assertEquals("9 * 8", n.prev());
		 // Assert.assertEquals("5 - 1", n.prev());
		 // Assert.assertEquals("1 + 2", n.prev());
		  Assert.assertEquals(null, n.prev());
		  n.load();
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals("8/4", n.current());
		  Assert.assertEquals("9*8", n.prev());
		  Assert.assertEquals("5-1", n.prev());
		  Assert.assertEquals("1+2", n.prev());
		  Assert.assertEquals(null, n.prev());*/
		  
		 /* n.input("2+3");
		  n.input("4+5");
		  Assert.assertEquals("2+3", n.prev());
		  Assert.assertEquals(null, n.prev());
		  Assert.assertEquals("2+3", n.current());
		  Assert.assertEquals("4+5", n.next());
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals("4+5", n.current());
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals("4+5", n.current());*/
		  
		 /* n.input("1+1");
		  n.input("2+2");
		  n.input("3+3");
		  n.input("4+4");
		  n.input("5+5");
		  n.input("6+6");
		
		  Assert.assertEquals("6+6", n.current());
		  Assert.assertEquals("5+5", n.prev());
		  Assert.assertEquals("4+4", n.prev());
		  Assert.assertEquals("3+3", n.prev());
		  Assert.assertEquals("2+2", n.prev());
		  Assert.assertEquals(null, n.prev());
		  Assert.assertEquals(null, n.prev());
		  Assert.assertEquals("2+2", n.current());
		  Assert.assertEquals("3+3", n.next());
		  Assert.assertEquals("4+4", n.next());
		  Assert.assertEquals("5+5", n.next());
		  Assert.assertEquals("6+6", n.next());
		  Assert.assertEquals(null, n.next());
		  Assert.assertEquals("6+6", n.current());	 
		  Assert.assertEquals("5+5", n.prev());
		  Assert.assertEquals("4+4", n.prev());
		  Assert.assertEquals("3+3", n.prev());
		  Assert.assertEquals("2+2", n.prev());
		  Assert.assertEquals(null, n.prev());*/
		  /*n.save();
		  n.load();
		  Assert.assertEquals("6+6", n.current());
		  Assert.assertEquals("5+5", n.prev());
		  Assert.assertEquals("4+4", n.prev());
		  Assert.assertEquals("3+3", n.prev());
		  Assert.assertEquals("2+2", n.prev());
		  Assert.assertEquals(null, n.prev());
		  n.input("7+7");
		  n.input("8+8");
		  Assert.assertEquals("8+8", n.current());
		  Assert.assertEquals("7+7", n.prev());
		  Assert.assertEquals(null, n.prev());
		  n.input("9+9");
		  n.input("10+10");
		  n.input("11+11");
		  Assert.assertEquals("11+11", n.current());
		  Assert.assertEquals("10+10", n.prev());
		  Assert.assertEquals("9+9", n.prev());
		  n.input("12+12");
		  n.input("13+13");
		  Assert.assertEquals("13+13", n.current());
		  Assert.assertEquals("12+12", n.prev());
		  Assert.assertEquals("8+8", n.prev());
		  Assert.assertEquals("7+7", n.prev());
		  Assert.assertEquals(null, n.prev());
			
		  
		/*  n.input("1+1");
		  n.input("2+2");
          n.next();
          n.next();
          Assert.assertEquals("2+2", n.current());*/
		  
		/*  n.input("1+1");
		  n.input("2+2");
		  n.input("3+3");
		  n.input("4+4");
		  n.input("5+5");
		  Assert.assertEquals("4+4", n.prev());
		  Assert.assertEquals("3+3", n.prev());
		  Assert.assertEquals("2+2", n.prev());
		  Assert.assertEquals("1+1", n.prev());
		  Assert.assertEquals(null, n.prev());
		  n.save();
		  n.load();
		  Assert.assertEquals("1+1", n.current());
		  Assert.assertEquals(null, n.prev());
		  Assert.assertEquals("1+1", n.current());
		  Assert.assertEquals(null, n.next());
		  */
		 /*   c.input("5+6");
			c.input("3+4");
			c.input("2+2");
			c.input("8+5");
			assertEquals("8+5", c.current());
			assertEquals(null, c.next());
			assertEquals("2+2", c.prev());
			assertEquals(Double.parseDouble("4.0"),
					Double.parseDouble(c.getResult()), 1e-19);
			assertEquals("3+4", c.prev());
			assertEquals("5+6", c.prev());
			assertEquals(null, c.prev());
			assertEquals("3+4", c.next());
			assertEquals("5+6", c.prev());
			assertEquals(null, c.prev());
			c.input("5+6");
			c.input("3+4");
			c.input("2+2");
			c.input("8+5");
			c.input("9+6");
			c.input("8+19");
			assertEquals(null, c.next());
			for (int counter = 0; counter < 5; counter++) {
				c.prev();
			}
			assertEquals(null, c.prev());
			/*c.input("fff + 5");
			try{
				c.getResult();
				fail("had to crash");
			}catch(RuntimeException e){}*/
 
		 n.input("1+1");
		 n.input("2+2");
		 n.input("3+3");
			try{
				n.load();
				fail("had to crash");
			}catch(RuntimeException e){}
		  
		  assertEquals("3+3", n.current());
      
 }

}
