package eg.edu.alexu.csd.oop.game;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Factory {

	private Shape tempShape;

	private static final Logger logger = LoggerFactory.getLogger(Factory.class);

	private static Factory newInstance = null;

	public static Factory getInstance() { // Singelton Pattern
		if (newInstance == null) {
			newInstance = new Factory();

		}
		return newInstance;
	}

	public Shape buildShape(String shapeName, int posX, int posY) {
		try {
			tempShape = createShape(shapeName);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			logger.error("Class not found exception ");
			logger.info("Error while searching for class in factory class ");
			e.printStackTrace();

		}
		tempShape.setX(posX);
		tempShape.setY(posY);

		return tempShape;
	}

	public Shape createShape(String shapeName)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> c = Class.forName("eg.edu.alexu.csd.oop.game." + shapeName);
		tempShape = (Shape) c.newInstance();
		return tempShape;

	}

	public String getClassName(GameObject tempShape) {
		String className = tempShape.getClass().getSimpleName();
		logger.info("Class name " + className);
		return className;
	}

}
