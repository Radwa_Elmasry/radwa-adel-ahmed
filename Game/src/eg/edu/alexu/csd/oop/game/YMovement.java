package eg.edu.alexu.csd.oop.game;

public class YMovement implements IStates {
	private static YMovement newInstance = null;
	private WorldImplementation world = WorldImplementation.getInstance(0, 0);
	private Shape currentMoving;

	public static YMovement getInstance() { // Singelton Pattern
		if (newInstance == null) {
			newInstance = new YMovement();

		}
		newInstance.run();
		return newInstance;
	}

	public void run() {
		updateObject();

	}

	public void updateObject() {
		world.getCurrentConstantObject();
		currentMoving = world.getCurrentMovingObject();
		currentMoving.setY((currentMoving.getY() + 2));
		if (currentMoving.getY() == world.getHeight()) {
			currentMoving.setX(-100);
			currentMoving.setY(-100);
		}
		setCurrentMovingObject();
	}

	public void setCurrentMovingObject() {
		world.setCurrentMovingObject(currentMoving);

	}

}
